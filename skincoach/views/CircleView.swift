//
//  CircleView.swift
//  skincoach
//
//  Created by Achin Kumar on 14/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

@IBDesignable class CircleView: UIView {
    @IBInspectable private var color = UIColor.Skintone.Ivory
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        set()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        set()
    }
    
    func set(color: UIColor? = nil) {
        if let color = color { self.color = color }
        
        subviews.forEach { $0.removeFromSuperview() }
        self.backgroundColor = UIColor.clear
        
        let minSide = frame.width > frame.height ? frame.height : frame.width
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0), radius: minSide/2.0, startAngle: 0, endAngle: CGFloat(Double.pi * 2), clockwise: true)
        
        let circleLayer = CAShapeLayer()
        circleLayer.path = circlePath.cgPath
        circleLayer.fillColor = self.color.cgColor
        circleLayer.strokeColor = UIColor.clear.cgColor
        
        layer.addSublayer(circleLayer)
    }
}
