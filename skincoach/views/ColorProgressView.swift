//
//  ColorProgressView.swift
//  skincoach
//
//  Created by Achin Kumar on 26/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

class ColorProgressView: UIView {
    private var progress: Float = 0
    private var progressView: UIView!
    private var trailingConstraint: NSLayoutConstraint!
    
    private static let DefaultTrackColor = UIColor.Common.LightGrey
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        set(trackColor: ColorProgressView.DefaultTrackColor, progress: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        set(trackColor: ColorProgressView.DefaultTrackColor, progress: 0)
    }
    
    func set(trackColor tc: UIColor? = nil, progress pro: Float? = nil) {
        if progressView == nil {
            progressView = UIView(frame: CGRect.zero)
            let constraints = addSubview(progressView, withMargins: (leading: 0, trailing: 0, top: 0, bottom: 0))
            trailingConstraint = constraints.trailing
        }
        
        if let trackColor = tc { backgroundColor = trackColor }
        
        if let progress = pro {
            self.progress = progress
            if self.progress < 0.0 { self.progress = 0.0 }
            if self.progress > 100.0 { self.progress = 100.0 }
            
            let antiProgress: Float = 100.0 - self.progress
            
            let trailingConstraingConstant = antiProgress == 0 ? CGFloat(0.0) : frame.width * CGFloat(antiProgress) / CGFloat(100.0)
            self.trailingConstraint.constant = -trailingConstraingConstant
            UIView.animate(withDuration: 0.2, animations: {
                self.progressView.backgroundColor = RiskLevel.forPercent(progress).color
                self.layoutIfNeeded()
            })
        }
    }
}

















