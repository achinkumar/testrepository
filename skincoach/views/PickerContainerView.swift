//
//  PickerContainerView.swift
//  skincoach
//
//  Created by Achin Kumar on 14/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

class PickerContainerView: BaseNibCustomView {
    @IBOutlet weak var backgroundView: UIButton!
    @IBOutlet weak var pickerContainer: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pickerContainerViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    private var data: [String]!
    private var selectedPos: Int!
    private var onDone: ((String, Int) -> Void)!
    private var onCancel: (() -> Void)?
    
    static func show(inViewController vc: UIViewController, withTitle title: String? = nil, withDisplayData data: [String], andSelectedValue selectedValue: String? = nil, withOnDone onDone: @escaping (String, Int) -> Void, andOnCancel onCancel: (() -> Void)? = nil) -> PickerContainerView {
        let pickerContainerView = PickerContainerView(frame: vc.view.frame)
        pickerContainerView.data = data
        pickerContainerView.selectedPos = data.index { $0 == selectedValue } ?? -1
        pickerContainerView.onDone = onDone
        if let onCancel = onCancel { pickerContainerView.onCancel = onCancel }
        vc.view.addSubview(pickerContainerView)
        pickerContainerView.setupPickerView()
        if let title = title { pickerContainerView.titleLabel.text = title }
        pickerContainerView.hideInstantly()
        Dispatch.after(seconds: 0.01) { pickerContainerView.show() }
        return pickerContainerView
    }
    
    @IBAction func done(_ sender: UIButton) {
        if selectedPos != -1 {
            onDone(data[selectedPos], selectedPos)
            hide()
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        hide()
        if let onCancel = onCancel { onCancel() }
    }
}

extension PickerContainerView: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedPos = row
        doneButton.isEnabled = true
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var attributes = [NSAttributedStringKey : Any]()
        attributes[NSAttributedStringKey.font] = UIFont(name: "AvenirNext-Regular", size: 15)!
        return NSAttributedString(string: data[row], attributes: attributes)
    }
}



extension PickerContainerView {
    private func setupPickerView() {
        if selectedPos == -1 { doneButton.isEnabled = false }
        pickerView.delegate = self
        pickerView.dataSource = self
        if selectedPos >= 0 { pickerView.selectRow(selectedPos, inComponent: 0, animated: false) }
    }
    
    private func show() {
        Animate.with(duration: 0.3) {
            self.pickerContainerViewBottomMargin.constant = 0
            self.backgroundView.alpha = 0.2
            self.layoutIfNeeded()
        }
    }
    
    func hide() {
        hide(inDuration: 0.3)
    }
    
    private func hideInstantly() {
        self.pickerContainerViewBottomMargin.constant = -260
        self.backgroundView.alpha = 0;
    }
    
    private func hide(inDuration duration: Float) {
        Animate.with(duration: duration, animations: {
            self.pickerContainerViewBottomMargin.constant = -260
            self.backgroundView.alpha = 0;
            self.layoutIfNeeded()
        }) { (x) in
            self.removeFromSuperview()
        }
    }
}





















