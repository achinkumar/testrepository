//
//  RadarView.swift
//  skincoach
//
//  Created by Achin Kumar on 12/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

class RadarView: UIView {
    private var color: UIColor = UIColor.Base.Primary
    private var strokeWidth: CGFloat = 7
    private var circles = [Circle]()
    private var forwardDirection = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        set()
        radiate()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        set()
        radiate()
    }
    
    func set(color: UIColor? = nil , strokeWidth: CGFloat? = nil) {
        subviews.forEach { $0.removeFromSuperview() }
        self.backgroundColor = UIColor.clear
        
        let minSide = frame.width > frame.height ? frame.height : frame.width
        
        circles.append(addCircle(ofRadius: ((2.0/10.0) * minSide) / 2.0).set {
            self.forwardDirection = true
            self.setNextCircles()
        })
        circles.append(addCircle(ofRadius: ((4/10.0) * minSide) / 2.0).set())
        circles.append(addCircle(ofRadius: ((6/10.0) * minSide) / 2.0).set())
        circles.append(addCircle(ofRadius: ((8/10.0) * minSide) / 2.0).set())
        circles.append(addCircle(ofRadius: minSide / 2.0).set {
            self.forwardDirection = false
            self.setNextCircles()
        })
    }
    
    func radiate() {
        forwardDirection = true
        setNextCircles()
        circles[0].blink()
    }
    
    private func addCircle(ofRadius radius: CGFloat) -> Circle {
        let circle = Circle(frame: CGRect(x: (frame.width / 2.0) - radius, y: (frame.height / 2.0) - radius, width: radius * 2.0, height: radius * 2.0))
        addSubview(circle)
        return circle
    }
    
    private func setNextCircles() {
        circles[0].nextCircle = circles[1]
        circles[1].nextCircle = circles[forwardDirection ? 2 : 0]
        circles[2].nextCircle = circles[forwardDirection ? 3 : 1]
        circles[3].nextCircle = circles[forwardDirection ? 4 : 2]
        circles[4].nextCircle = circles[3]
    }
}

class Circle: UIView {
    var color: UIColor = UIColor.Base.Primary
    var strokeWidth: CGFloat = 7
    var animationDuration: Float = 1.0
    var onBlinkComplete: (()->Void)? = nil
    var nextCircle: Circle? = nil
    
    func set(color: UIColor? = nil , strokeWidth: CGFloat? = nil, animationDuration: Float? = nil, onBlinkComplete: (()->Void)? = nil) -> Circle {
        if let color = color { self.color = color }
        if let strokeWidth = strokeWidth { self.strokeWidth = strokeWidth }
        if let animationDuration = animationDuration { self.animationDuration = animationDuration }
        if let onBlinkComplete = onBlinkComplete { self.onBlinkComplete = onBlinkComplete }
        
        subviews.forEach { $0.removeFromSuperview() }
        self.backgroundColor = UIColor.clear
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0), radius: (frame.size.width - self.strokeWidth)/2, startAngle: 0, endAngle: CGFloat(Double.pi * 2), clockwise: true)
        
        let circleLayer = CAShapeLayer()
        circleLayer.path = circlePath.cgPath
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.strokeColor = self.color.cgColor
        circleLayer.lineWidth = CGFloat(self.strokeWidth)
        
        layer.addSublayer(circleLayer)
        
        alpha = 0.2
        
        return self
    }
    
    func blink() {
        UIView.animate(withDuration: TimeInterval(animationDuration/2.0), delay: 0, options: [.curveLinear], animations: {
            self.alpha = 1.0
        }) { (x) in
            if let onBlinkComplete = self.onBlinkComplete { onBlinkComplete() }
        }
        
        Dispatch.after(seconds: animationDuration/4) {
            UIView.animate(withDuration: TimeInterval(self.animationDuration/2.0), delay: 0, options: [.curveEaseIn], animations: {
                self.alpha = 0.2
            }) { (x) in
                
            }
        }
        
        Dispatch.after(seconds: animationDuration / 1.5) {
            if let nextCircle = self.nextCircle {
                nextCircle.blink()
            }
        }
    }
}
