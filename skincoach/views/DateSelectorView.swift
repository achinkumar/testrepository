//
//  DateSelectorView.swift
//  skincoach
//
//  Created by Achin Kumar on 14/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

class DateSelectorView: BaseNibCustomView {
    
    @IBOutlet weak var backgroundView: UIButton!
    @IBOutlet weak var datePickerContainer: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerContainerBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    var maxDate: Date?
    var minDate: Date?
    var selectedDate: Date = Date().minus(years: 20)
    private var onDone: ((Date) -> Void)!
    private var onCancel: (() -> Void)?
    
    static func show(inViewController vc: UIViewController, withTitle title: String? = nil, withSelectedDate selectedDate: Date? = nil, andMaxDate maxDate: Date? = nil, andMinDate minDate: Date? = nil, withOnDone onDone: @escaping (Date) -> Void, andOnCancel onCancel: (() -> Void)? = nil) -> DateSelectorView {
        
        let dateSelectorView = DateSelectorView(frame: vc.view.frame)
        if let maxDate = maxDate { dateSelectorView.maxDate = maxDate }
        if let minDate = minDate { dateSelectorView.minDate = minDate }
        if let selectedDate = selectedDate { dateSelectorView.selectedDate = selectedDate }
        if let onCancel = onCancel { dateSelectorView.onCancel = onCancel }
        dateSelectorView.onDone = onDone
        
        vc.view.addSubview(dateSelectorView)
        
        dateSelectorView.setup()
        dateSelectorView.titleLabel.text = title
        dateSelectorView.hideInstantly()
        Dispatch.after(seconds: 0.01) { dateSelectorView.show() }
        return dateSelectorView
    }
    
    @IBAction func onDone(_ sender: UIButton) {
        hide()
        onDone(selectedDate)
    }
    
    @IBAction func onCancel(_ sender: UIButton) {
        hide()
        if let onCancel = onCancel { onCancel() }
    }
    
    @IBAction func onSelection(_ sender: UIDatePicker) {
        selectedDate = sender.date
    }
}

extension DateSelectorView {
    private func setup() {
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        datePicker.date = selectedDate
    }
    
    private func show() {
        Animate.with(duration: 0.3) {
            self.datePickerContainerBottomMargin.constant = 0
            self.backgroundView.alpha = 0.2
            self.layoutIfNeeded()
        }
    }
    
    func hide() {
        hide(inDuration: 0.3)
    }
    
    private func hideInstantly() {
        self.datePickerContainerBottomMargin.constant = -260
        self.backgroundView.alpha = 0;
    }
    
    private func hide(inDuration duration: Float) {
        Animate.with(duration: duration, animations: {
            self.datePickerContainerBottomMargin.constant = -260
            self.backgroundView.alpha = 0;
            self.layoutIfNeeded()
        }) { (x) in
            self.removeFromSuperview()
        }
    }
}
