//
//  NDEFReaderDelegate.swift
//  skincoach
//
//  Created by Achin Kumar on 20/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit
import CoreNFC

class NDEFReader: NSObject, NFCNDEFReaderSessionDelegate {
    private let CapVoltage: Float = 450             // mV
    private let CapReading: Float = 16383           // decimal
    
    private var initialReading: Reading?
    private var nfcSession: NFCNDEFReaderSession?
    private var hasManuallyInvalidated = false
    
    private var onResult: ((ReadingPair) -> Void)?
    private var onError: ((String) -> Void)?
    
    private static var singleton: NDEFReader?
    
    static var instance: NDEFReader {
        if singleton == nil {
            singleton = NDEFReader()
        } else {
            singleton!.hasManuallyInvalidated = true
            singleton!.nfcSession?.invalidate()
        }
        return singleton!
    }
    
    private var manualDelay: Float?
    
    func read(forSeconds seconds: Float? = nil, onResult: @escaping (ReadingPair) -> Void, onError: @escaping (String) -> Void) {
        self.onResult = onResult
        self.onError = onError
        self.manualDelay = seconds
        
        hasManuallyInvalidated = false
        initialReading = nil            // TODO: Can be integrated with a caching logic around initial reading
        nfcSession = NFCNDEFReaderSession.init(delegate: self, queue: DispatchQueue.main, invalidateAfterFirstRead: false)
        nfcSession?.alertMessage = "Place and hold your phone on top of the sensor."
        nfcSession?.begin()
    }
    
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        print("The session was invalidated: \(error.localizedDescription)")
        if hasManuallyInvalidated {
            hasManuallyInvalidated = false
        } else if let onError = onError {
            hasManuallyInvalidated = true
            session.invalidate()
//            onError("Reading failed. Please try again.")
        }
    }
    
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        func delayForVoltage(_ voltage: Int) -> Float {
            let maxDelay: Float = 15.0
            let delay = (Float(voltage) / CapVoltage) * maxDelay
            return delay > 2.0 ? delay : 2.0
        }
        
        func updateAlert(forDelay delay: Float) {
            let seconds = Int(delay)
            let milliseconds = Int((delay - Float(seconds)) * 100)
            let stamp = "\(seconds) : \(milliseconds)"
            session.alertMessage = stamp
            var delay = delay - 0.01
            if delay > 0.01 {
                if delay < 0 { delay = 0 }
                Dispatch.after(seconds: 0.01) {
                    updateAlert(forDelay: delay)
                }
            }
        }
        
        var delay: Float = 0
        
        if let reading = readingFromMessage(messages[0]) {
            if initialReading == nil {
                initialReading = reading
                session.alertMessage = "Keep holding your phone on top of the sensor"
                
                delay = manualDelay ?? delayForVoltage(reading.adc0)
                
                if Config.manualInterventionSupported { updateAlert(forDelay: delay) }
                
                Dispatch.after(seconds: Config.manualInterventionSupported ? delay * 1.13 : delay) {
                    self.hasManuallyInvalidated = true
                    session.invalidate()
                    Dispatch.after(seconds: 0.1) {
                        self.nfcSession = NFCNDEFReaderSession.init(delegate: self, queue: DispatchQueue.main, invalidateAfterFirstRead: false)
                        self.nfcSession?.alertMessage = "Keep holding your phone on top of the sensor"
                        self.nfcSession?.begin()
                    }
                }
            } else {
                let readingPair = ReadingPair(initialReading: initialReading!, finalReading: reading)
                readingPair.save()
                readingPair.saveAsLast()
                hasManuallyInvalidated = true
                session.invalidate()
                if let onResult = onResult { onResult(readingPair) }
            }
        } else {
            session.invalidate()
            if let onError = onError { onError("Reading failed. Please try again.") }
        }
    }
    
    
    private func readingFromMessage(_ message: NFCNDEFMessage) -> Reading? {
        let payload = message.records[0]
        let chars = Array(payload.payload).map { String(format:"%02hhx", $0) }
        print(chars)
        
        let strInitFirst  = chars.count > 9 ? "\(chars[9])" : ""
        let strInitSecond = chars.count > 8 ? "\(chars[8])" : ""
        
        let adcDecimal = hexToDecimal(hex: "\(strInitFirst)\(strInitSecond)")
        let adc = Int((Float(adcDecimal) / CapReading) * CapVoltage)           // mV
        
        return Reading(adc0: adc, pass: initialReading == nil ? Pass.BEFORE : Pass.AFTER)
    }
    
    private func hexToDecimal(hex: String) -> Int {
        return Int(hex, radix: 16)!
    }
}























