//
//  Locator.swift
//  skincoach
//
//  Created by Achin Kumar on 22/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import CoreLocation

class Locator: NSObject, CLLocationManagerDelegate {
    private var locationManager:CLLocationManager!
    private var onLocation: ((CLLocation?, String?) -> Void)?
    
    func locate(_ onLocation: ((CLLocation?, String?) -> Void)? = nil) {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            self.onLocation = onLocation
            locationManager.startUpdatingLocation()
        } else {
            onLocation!(nil, "Location permission not granted")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        manager.stopUpdatingLocation()
        if let onLocation = onLocation {
            onLocation(userLocation, nil)
            self.onLocation = nil
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let onLocation = onLocation {
            onLocation(nil, "Error: \(error)")
            self.onLocation = nil
        }
    }
}
