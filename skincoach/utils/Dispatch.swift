//
//  Dispatch.swift
//  skincoach
//
//  Created by Achin Kumar on 09/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import Foundation

func BG(_ block: @escaping ()->Void) {
    DispatchQueue.global(qos: .default).async(execute: block)
}

func UI(_ block: @escaping ()->Void) {
    DispatchQueue.main.async(execute: block)
}

class Dispatch {
    static func after(seconds: Float, execute work: @escaping @convention(block) () -> Swift.Void) {
        let milliseconds = Int(seconds * 1000)
        DispatchQueue.main.asyncAfter(deadline: .now() + DispatchTimeInterval.milliseconds(milliseconds)) {
            work()
        }
    }
    
    static func onMain(work: @escaping @convention(block) () -> Swift.Void) {
        DispatchQueue.main.async() {
            work()
        }
    }
    
    static func eoq(work: @escaping @convention(block) () -> Swift.Void) {
        after(seconds: 0, execute: work)
    }
}
