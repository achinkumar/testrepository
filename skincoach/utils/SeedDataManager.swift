//
//  SeedDataManager.swift
//  skincoach
//
//  Created by Achin Kumar on 26/02/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation

class MockedLocation {
    let address: String
    let temperature: Float
    let humidity: Float
    let uvIndex: Int
    let avgOutTime: Int
    let latitude: Float
    let longitude: Float
    
    init(address: String, temperature: Float, humidity: Float, uvIndex: Int, avgOutTime: Int, latitude: Float, longitude: Float) {
        self.address = address
        self.temperature = temperature
        self.humidity = humidity
        self.uvIndex = uvIndex
        self.avgOutTime = avgOutTime
        self.latitude = latitude
        self.longitude = longitude
    }
    
    var randomCondition: Conditions {
        let conditions = Conditions()
        conditions.humidity = Double(humidity + Float.random(min: -5, max: 5)/100.0)
        conditions.temperature = Double(temperature + Float.random(min: -5, max: 5))
        conditions.uvIndex = abs(uvIndex + Int.random(min: -2, max: 2))
        return conditions
    }
    
    var location: CLLocation { return CLLocation(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude)) }
    
    func adc0Gain(forOutTime outTime: Int) -> Int { return Int((Float(outTime) / 5.0) * (Float(uvIndex) / 3.0)) }
    
    static let SanFrancisco = MockedLocation(
        address: "San Francisco, US",
        temperature: 50,
        humidity: 0.82,
        uvIndex: 1,
        avgOutTime: 150,
        latitude: 37.7749,
        longitude: -122.4194
    )
    static let Paris = MockedLocation(
        address: "Paris, FR",
        temperature: 32,
        humidity: 0.77,
        uvIndex: 1,
        avgOutTime: 180,
        latitude: 48.8566,
        longitude: 2.3522
    )
    static let Sydney = MockedLocation(
        address: "Sydney, AUS",
        temperature: 77,
        humidity: 0.71,
        uvIndex: 6,
        avgOutTime: 120,
        latitude: -33.8688,
        longitude: 151.2093
    )
    static let LasVegas = MockedLocation(
        address: "Las Vegas, US",
        temperature: 45,
        humidity: 0.35,
        uvIndex: 1,
        avgOutTime: 100,
        latitude: 36.1699,
        longitude: -115.1398
    )
    
    static func forDate(_ date: Date) -> MockedLocation {
        let daysFromToday = abs(date.daysFromToday)
        if daysFromToday < 3 { return LasVegas }
        if daysFromToday < 15 { return Sydney }
        if daysFromToday < 27 { return Paris }
        return SanFrancisco
    }
}


class SeedDataManager {
    let realm: Realm
    var baseline = 10
    var lastReadingPair: ReadingPair? = nil
    
    init(realm: Realm) { self.realm = realm }
    
    func seedData(forDays days: UInt) {
        var date = Date().minus(days: days).startOfDay
        repeat {
            addData(forDate: date)
            date = date.plus(days: 1).startOfDay
        } while !date.same(date: Date())
        print("Seeding complete")
    }
    
    private func addData(forDate date: Date) {
        var dataPoints = (0...Int.random(n: 5)).map { _ in Int.random(n: 23) }.unique.sorted()
        if dataPoints.count == 0 {
            dataPoints = (0...Int.random(n: 5)).map { _ in Int.random(n: 23) }.unique.sorted()
        }
        for i in 0..<dataPoints.count {
            let dateCopy = date.copy(hour: dataPoints[i], minute: Int.random(n: 59))
            (i == 0 || Bool.random) ? addSync(forDate: dateCopy) : addActivity(forDate: dateCopy)
        }
    }
    
    private func addActivity(forDate date: Date) {
        let location = MockedLocation.forDate(date)
        let conditions = location.randomCondition
        
        var outTime = Int.random(n: 30)
        baseline += location.adc0Gain(forOutTime: outTime)
        
        let firstInitialReading = Reading(adc0: baseline, pass: .BEFORE)
        firstInitialReading.save(in: realm)
        
        baseline = 10.shift()
        
        let firstFinalReading = Reading(adc0: baseline, pass: .AFTER)
        firstFinalReading.save(in: realm)
        save(readingPair: ReadingPair(initialReading: firstInitialReading, finalReading: firstFinalReading, conditions: conditions, address: location.address, location: location.location, date: date))
        
        let fromUv = firstFinalReading.uva + firstFinalReading.uvb
        
        outTime = location.avgOutTime.shift(30)
        baseline += location.adc0Gain(forOutTime: outTime)
        
        let endDate = date.plus(minutes: UInt(outTime))
        
        let secondInitialReading = Reading(adc0: baseline, pass: .BEFORE)
        secondInitialReading.save(in: realm)
        
        let toUv = secondInitialReading.uva + secondInitialReading.uvb
        
        baseline = 10.shift()
        
        let secondFinalReading = Reading(adc0: baseline, pass: .AFTER)
        secondFinalReading.save(in: realm)
        
        let activity = Activity.random
        
        PerformedActivity(activityId: activity.id,
                          uvFrom: fromUv,
                          uvTo: toUv,
                          date: endDate,
                          timeOfDay: date.timeOfDay,
                          address: location.address,
                          startConditions: conditions,
                          endConditions: conditions,
                          lat: Double(location.latitude),
                          lng: Double(location.longitude),
                          startedAtMS: Double(date.millisecondsSince1970),
                          stoppedAtMS: Double(endDate.millisecondsSince1970))
            .save(in: realm)

        save(readingPair: ReadingPair(initialReading: secondInitialReading, finalReading: secondFinalReading, conditions: conditions, address: location.address, location: location.location, date: endDate))
    }
    
    private func addSync(forDate date: Date) {
        let location = MockedLocation.forDate(date)
        let conditions = location.randomCondition
        let outTime = abs(location.avgOutTime + Int.random(n: 80))
        let adc0Gain = location.adc0Gain(forOutTime: outTime)
        baseline += adc0Gain
        
        let initialReading = Reading(adc0: baseline, pass: .BEFORE)
        initialReading.save(in: realm)
        
        baseline = 10.shift()
        
        let finalReading = Reading(adc0: baseline, pass: .BEFORE)
        finalReading.save(in: realm)
        
        save(readingPair: ReadingPair(initialReading: initialReading, finalReading: finalReading, conditions: conditions, address: location.address, location: location.location, date: date))
    }
    
    private func save(readingPair: ReadingPair) {
        if let lastReadingPair = lastReadingPair {
            let realm = try! Realm()
            try! realm.write {
                lastReadingPair.isLast = false
            }
            if lastReadingPair.date.same(date: readingPair.date) {
                readingPair.uvGain = readingPair.initialReading!.uva + readingPair.initialReading!.uvb - lastReadingPair.finalReading!.uva - lastReadingPair.finalReading!.uvb
            }
            if readingPair.uvGain < 1E-10 { readingPair.uvGain = 0.0 }
        }
        readingPair.isLast = true
        readingPair.save(in: realm)
        lastReadingPair = readingPair
    }
}






