//
//  Cache.swift
//  skincoach
//
//  Created by Achin Kumar on 23/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import RealmSwift
import CoreLocation

class Cache: Object {
    private static let DEFAULT_CACHE_ID = "__default__cache__"
    
    @objc dynamic var id: String = DEFAULT_CACHE_ID
    @objc dynamic var raw_latitude: Double = 0.0
    @objc dynamic var raw_longitude: Double = 0.0
    @objc dynamic var raw_address: String?
    @objc dynamic var raw_conditions: Conditions?
    
    @objc dynamic var raw_location_time = Date().minus(years: 1)
    @objc dynamic var raw_address_time = Date().minus(years: 1)
    @objc dynamic var raw_conditions_time = Date().minus(years: 1)
    
    private static let MIN_TTL = 60 * 60                        // 1 hour
    private static let MAX_TTL = 8 * 60 * 60                    // 8 hours
    
    private var locator: Locator!
    
    override static func primaryKey() -> String? { return "id" }
    override static func ignoredProperties() -> [String] { return ["locator"] }
    
    var locationIsStale: Bool { return abs(Date.secondsBetween(date1: Date(), date2: raw_location_time)) > Cache.MAX_TTL }
    var addressIsStale: Bool { return abs(Date.secondsBetween(date1: Date(), date2: raw_address_time)) > Cache.MAX_TTL }
    var conditionsIsStale: Bool { return abs(Date.secondsBetween(date1: Date(), date2: raw_conditions_time)) > Cache.MAX_TTL }
    
    var shouldUpdate: Bool {
        let now = Date()
        return abs(Date.secondsBetween(date1: now, date2: raw_location_time)) > Cache.MIN_TTL
            || abs(Date.secondsBetween(date1: now, date2: raw_address_time)) > Cache.MIN_TTL
            || abs(Date.secondsBetween(date1: now, date2: raw_conditions_time)) > Cache.MIN_TTL
    }
    
    var location: CLLocation? {
        get {
            if raw_latitude != 0.0 && raw_longitude != 0.0 && !locationIsStale {
                return CLLocation(latitude: raw_latitude, longitude: raw_longitude)
            } else { return nil }
        }
        
        set {
            if let location = newValue {
                Realm.write {
                    raw_latitude = location.coordinate.latitude
                    raw_longitude = location.coordinate.longitude
                    raw_location_time = Date()
                }
            }
        }
    }
    
    var address: String? {
        get {
            if let address = raw_address, !addressIsStale { return address }
            else { return nil }
        }
        
        set {
            if let address = newValue {
                Realm.write {
                    raw_address = address
                    raw_address_time = Date()
                }
            }
        }
    }
    
    var conditions: Conditions? {
        get {
            if let conditions = raw_conditions, !conditionsIsStale { return conditions }
            else { return nil }
        }
        
        set {
            if let conditions = newValue {
                Realm.write {
                    raw_conditions = Conditions()
                    raw_conditions!.temperature = conditions.temperature
                    raw_conditions!.humidity = conditions.humidity
                    raw_conditions!.pressure = conditions.pressure
                    raw_conditions!.windSpeed = conditions.windSpeed
                    raw_conditions!.uvIndex = conditions.uvIndex
                    raw_conditions_time = Date()
                }
            }
        }
    }
    
    static var def: Cache {
        if let defaultCache = Cache.one() { return defaultCache }
        let cache = Cache()
        cache.save()
        return cache
    }
    
    static func refresh(onDone: ((Bool) -> Void)? = nil) { def.refresh(onDone: onDone) }
    
    private func refresh(onDone: ((Bool) -> Void)? = nil) {
        if !shouldUpdate { onDone?(false) }
        else {
            locator = Locator()
            locator.locate { (location, error) in
                if let location = location {
                    Realm.write { self.location = location }
                    var hasUpdatedAddress = false, hasUpdatedConditions = false
                    location.address { address in
                        Realm.write { self.address = address }
                        hasUpdatedAddress = true
                        if hasUpdatedConditions { onDone?(true) }
                    }
                    DarkSkyClient().conditions(forLocation: location) { conditions in
                        Realm.write { self.conditions = conditions }
                        hasUpdatedConditions = true
                        if hasUpdatedAddress { onDone?(true) }
                    }
                } else {
                    onDone?(false)
                }
            }
        }
    }
}
