//
//  Animate.swift
//  skincoach
//
//  Created by Achin Kumar on 12/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

class Animate {
    static func with(duration: Float, animations: @escaping () -> Swift.Void) {
        UIView.animate(withDuration: TimeInterval(0.25), delay: 0, options: [.curveEaseInOut], animations: {
            animations()
        }, completion: nil)
    }
    
    static func with(duration: Float, animations: @escaping () -> Swift.Void, completion: ((Bool) -> Swift.Void)? = nil) {
        UIView.animate(withDuration: TimeInterval(0.25), delay: 0, options: [.curveEaseInOut], animations: animations, completion: completion)
    }
}
