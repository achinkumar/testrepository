//
//  BaseAnimator.swift
//  skincoach
//
//  Created by Achin Kumar on 13/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

class BaseAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var type : UINavigationControllerOperation = .push
    
    init(operation: UINavigationControllerOperation) {
        super.init()
        type = operation
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.7
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        assert(false, "animateTransition - method needs to be overridden in subclass")
    }
}

