//
//  SlideOverBottomAnimator.swift
//  skincoach
//
//  Created by Achin Kumar on 13/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

class SlideOverBottomAnimator: BaseAnimator {
    override func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1.0
    }
    
    override func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let fromVC = transitionContext.viewController(forKey: .from)
        let toVC = transitionContext.viewController(forKey: .to)
        
        if self.type == .push {
            containerView.insertSubview((toVC?.view)!, aboveSubview: (fromVC?.view)!)
            toVC?.view.transform = CGAffineTransform(translationX: 0, y: 1000)
            
            UIView.animate(withDuration: self.transitionDuration(using: transitionContext),
                           delay: 0.0,
                           usingSpringWithDamping: 1.0,
                           initialSpringVelocity: 1.0,
                           options: [.curveLinear],
                           animations: {
                            toVC?.view.transform = CGAffineTransform.identity
            }, completion: {(_) in
                fromVC?.view.removeFromSuperview()
                transitionContext.completeTransition(true)
            })
            
        } else if self.type == .pop {
            containerView.insertSubview((toVC?.view)!, belowSubview: (fromVC?.view)!)
            UIView.animate(withDuration: self.transitionDuration(using: transitionContext),
                           delay: 0.0,
                           usingSpringWithDamping: 1.0,
                           initialSpringVelocity: 1.0,
                           options: [.curveLinear],
                           animations: {
                            fromVC?.view.transform = CGAffineTransform(translationX: 0, y: 1000)
            }, completion: {(_) in
                fromVC?.view.removeFromSuperview()
                transitionContext.completeTransition(true)
            })
        }
    }
}
