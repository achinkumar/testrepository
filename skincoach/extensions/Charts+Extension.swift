//
//  Charts+Extension.swift
//  skincoach
//
//  Created by Achin Kumar on 15/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import Charts
import UIKit

extension LineChartView {
    func setup(entries: [ChartDataEntry], gridColor: UIColor = UIColor.Base.Primary, lineColors: [UIColor] = [UIColor.Base.Primary], yMin: Float = 0.0, yMax: Float = 100.0) {
        let uvLine = LineChartDataSet(values: entries, label: "")
        
        if lineColors.count > 1 {
            let maxEntry = Float(entries.map { $0.y }.max()!)
            let color1 = lineColors[0]
            let color2 = lineColors[0].mixin(infusion: lineColors[1], alpha: CGFloat(maxEntry / yMax))
            uvLine.colors = [color1, color2]
            uvLine.isDrawLineWithGradientEnabled = true
            uvLine.gradientPositions = [CGFloat(0), CGFloat(1)]
        } else {
            uvLine.colors = lineColors
        }
        
        uvLine.mode = LineChartDataSet.Mode.horizontalBezier
        uvLine.lineWidth = 2.5
        uvLine.drawCircleHoleEnabled = true
        uvLine.drawCirclesEnabled = false
        uvLine.drawHorizontalHighlightIndicatorEnabled = false
        uvLine.setDrawHighlightIndicators(false)
        uvLine.drawValuesEnabled = false
        
        xAxis.labelPosition = XAxis.LabelPosition.bottom
        xAxis.labelTextColor = UIColor.clear
        xAxis.drawGridLinesEnabled = true
        xAxis.drawAxisLineEnabled = false
        xAxis.gridLineWidth = 0.7
        xAxis.gridColor = gridColor
        
        rightAxis.drawLabelsEnabled = false
        rightAxis.drawAxisLineEnabled = false
        rightAxis.drawGridLinesEnabled = true
        rightAxis.gridLineWidth = 0.7
        rightAxis.gridColor = gridColor
        
        leftAxis.drawLabelsEnabled = false
        leftAxis.drawAxisLineEnabled = false
        leftAxis.drawGridLinesEnabled = true
        leftAxis.gridColor = UIColor.clear
        leftAxis.axisMinimum = Double(yMin - 1)
        leftAxis.axisMaximum = Double(yMax + 1)
        
        
        let data = LineChartData()
        data.addDataSet(uvLine)
        chartDescription?.text = ""
        
        pinchZoomEnabled = false
        setScaleEnabled(false)
        
        legend.enabled = false
        setViewPortOffsets(left: 0.0, top: 0.0, right: 0.0, bottom: 0.0)
        
        self.data = data
        layoutIfNeeded()
    }
}


extension BarChartView {
    func setup(entries: [ChartDataEntry], barColor: UIColor = UIColor.Base.PrimaryDark, barColorRisky: UIColor = UIColor.Risk.Critical, inactiveBarColor: UIColor = UIColor.Chart.BarBackground, gridColor: UIColor = UIColor.clear, yMin: Float = 0.0, yMax: Float = 100.0) {
        
        let dataSetBackground = BarChartDataSet(values: entries.map { BarChartDataEntry(x: $0.x, y: Double(yMax)) }, label: "")
        dataSetBackground.colors = [inactiveBarColor]
        
        let dataSet = ColoredBarDataSet(values: entries.map { BarChartDataEntry(x: $0.x, y: $0.y) }, label: "")
        dataSet.colors = [barColor, barColorRisky]
        
        dataSet.drawValuesEnabled = false
        
        xAxis.labelPosition = XAxis.LabelPosition.bottom
        xAxis.labelTextColor = UIColor.clear
        xAxis.drawGridLinesEnabled = true
        xAxis.drawAxisLineEnabled = false
        xAxis.gridLineWidth = 0.7
        xAxis.gridColor = gridColor
        
        rightAxis.drawLabelsEnabled = false
        rightAxis.drawAxisLineEnabled = false
        rightAxis.drawGridLinesEnabled = true
        rightAxis.gridLineWidth = 0.7
        rightAxis.gridColor = gridColor
        
        leftAxis.drawLabelsEnabled = false
        leftAxis.drawAxisLineEnabled = false
        leftAxis.drawGridLinesEnabled = true
        leftAxis.gridColor = UIColor.clear
        leftAxis.axisMinimum = Double(yMin - 1)
        leftAxis.axisMaximum = Double(yMax + 1)
        
        let barData = BarChartData(dataSets: [dataSetBackground, dataSet])
        data = barData
        
        pinchZoomEnabled = false
        setScaleEnabled(false)
        legend.enabled = false
        chartDescription?.text = ""
        
        setViewPortOffsets(left: 0, top: 0, right: 0, bottom: 0)
        
        layoutIfNeeded()
    }
}


class ColoredBarDataSet : BarChartDataSet {
    override func color(atIndex index: Int) -> NSUIColor {
        return entryForIndex(index)!.y < 100 ? colors[0] : colors[1]
    }
}



