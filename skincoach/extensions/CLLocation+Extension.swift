//
//  CLLocation+Extension.swift
//  skincoach
//
//  Created by Achin Kumar on 22/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import CoreLocation

extension CLLocation {
    func address(_ onResult: @escaping (String) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(self) { (placemarks, error) in
            if let placemarks = placemarks { onResult("\(placemarks[0].locality!), \(placemarks[0].isoCountryCode!)") }
            else { return onResult("") }
        }
    }
}
