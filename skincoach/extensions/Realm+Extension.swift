//
//  Realm+Extension.swift
//  skincoach
//
//  Created by Achin Kumar on 26/02/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import Foundation
import RealmSwift

extension Realm {
    static func write(in realm: Realm? = nil, _ writeBlock: () -> Void) {
        let realm = try! realm ?? Realm()
        if realm.isInWriteTransaction {
            writeBlock()
        } else {
            try! realm.write {
                writeBlock()
            }
        }
    }
}

extension Object {
    func save(in realm: Realm? = nil) {
        let realm = try! realm ?? Realm()
        try! realm.write {
            realm.add(self, update: true)
        }
    }
    
    static func saveAll<T>(objs: [T], in realm: Realm? = nil) where T: Object {
        let realm = try! realm ?? Realm()
        try! realm.write {
            realm.add(objs)
        }
    }
    
    static func one(where predicate: String? = nil) -> Self? {
        let realm = try! Realm()
        var objs = realm.objects(self)
        if let predicate = predicate { objs = objs.filter(predicate) }
        if objs.count > 0 {
            return objs[0]
        }
        return nil
    }

    static func all(where predicate: String? = nil) -> [Object] {
        let realm = try! Realm()
        var objs = realm.objects(self)
        if let predicate = predicate { objs = objs.filter(predicate) }
        return objs.map { return $0 }
    }
}


extension Array where Element == Object {
    func save() { Object.saveAll(objs: self)}
}
