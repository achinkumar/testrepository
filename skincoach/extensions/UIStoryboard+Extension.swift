//
//  UIStoryboard+Extension.swift
//  skincoach
//
//  Created by Achin Kumar on 13/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

extension UIStoryboard {
    // Main Storyboard Instance
    static var mainStoryboard: UIStoryboard { return UIStoryboard(name: Constants.MainStoryboardName, bundle: Bundle.main) }
    
    static func vc<T>(_ type:T.Type) -> (T) where T: UIViewController {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: T.self)) as! T
    }
}

extension UIStoryboard {
    fileprivate struct Constants {
        static let MainStoryboardName = "Main"
    }
}
