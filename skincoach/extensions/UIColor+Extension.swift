//
//  UIColor+Extension.swift
//  skincoach
//
//  Created by Achin Kumar on 28/02/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        let newRed   = CGFloat(Double(red) / 255.0)
        let newGreen = CGFloat(Double(green) / 255.0)
        let newBlue  = CGFloat(Double(blue) / 255.0)
        
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: CGFloat(1.0))
    }
    
    convenience init(fromHexString hex: String, andAlpha alpha: Float = 1.0) {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            self.init(red: 0, green: 0, blue: 0)
        } else {
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            
            self.init(
                red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                alpha: CGFloat(alpha)
            )
        }
    }
    
    func clone() -> UIColor {
        var (red, green, blue, alpha) = (CGFloat(0.0), CGFloat(0.0), CGFloat(0.0), CGFloat(0.0))
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: red, green: green, blue: blue, alpha: alpha)
        }
        return UIColor.white
    }
}

extension UIColor {
    struct Base {
        static var PrimaryIcons : UIColor { return UIColor(fromHexString: "#00bff1") }
        static var Primary      : UIColor { return UIColor(fromHexString: "#71BCFA") }
        static var PrimaryDark  : UIColor { return UIColor(fromHexString: "#00aeed") }
        static var PrimaryLight : UIColor { return UIColor(fromHexString: "#cbeffb") }
        static var Accent       : UIColor { return UIColor(fromHexString: "#B3B3B3") }
        static var LightBlue    : UIColor { return UIColor(fromHexString: "#ddf4fd") }
        static var DarkBlue     : UIColor { return UIColor(fromHexString: "#0079a5") }
        static var LightBlueAlt : UIColor { return UIColor(fromHexString: "#c8eafa") }
    }
    
    struct Activity {
        static var BocceBall    : UIColor { return UIColor(fromHexString: "#96ddf7") }
        static var Basketball   : UIColor { return UIColor(fromHexString: "#00bff1") }
        static var Gardening    : UIColor { return UIColor(fromHexString: "#82cb9e") }
        static var Kayaking     : UIColor { return UIColor(fromHexString: "#dcbb15") }
        static var Running      : UIColor { return UIColor(fromHexString: "#f16950") }
        static var Swimming     : UIColor { return UIColor(fromHexString: "#96ddf7") }
        static var Tennis       : UIColor { return UIColor(fromHexString: "#00bff1") }
        static var Walking      : UIColor { return UIColor(fromHexString: "#82cb9e") }
    }
    
    struct Skintone {
        static var Ivory    : UIColor { return UIColor(fromHexString: "#F6EBD7") }
        static var Fair     : UIColor { return UIColor(fromHexString: "#F3E3CA") }
        static var Beige    : UIColor { return UIColor(fromHexString: "#DCC591") }
        static var Olive    : UIColor { return UIColor(fromHexString: "#B08B52") }
        static var Dark     : UIColor { return UIColor(fromHexString: "#6C4C25") }
        static var VeryDark : UIColor { return UIColor(fromHexString: "#4B2C0D") }
    }
    
    struct Risk {
        static var Low      : UIColor { return UIColor(fromHexString: "#66B510") }
        static var Moderate : UIColor { return UIColor(fromHexString: "#FF6D00") }
        static var High     : UIColor { return UIColor(fromHexString: "#F91F3A") }
        static var Critical : UIColor { return UIColor(fromHexString: "#D0021B") }
    }
    
    struct Chart {
        static var LightYellow      : UIColor { return UIColor(fromHexString: "#fad961") }
        static var DarkOrange       : UIColor { return UIColor(fromHexString: "#f76b1c") }
        static var BarBackground    : UIColor { return UIColor(fromHexString: "#e5e5e5", andAlpha: 0.4) }
    }
    
    struct Common {
        static var OffWhite     : UIColor { return UIColor(fromHexString: "#f5f5f5") }
        static var LightGrey    : UIColor { return UIColor(fromHexString: "#dddddd") }
        static var TrackColor   : UIColor { return UIColor(fromHexString: "#D6E6ED") }
    }
    
    struct Gradient {
        static var background: CAGradientLayer {
            let gl = CAGradientLayer()
            gl.colors = [ UIColor.white, UIColor.Base.LightBlue ]
            gl.locations = [ 0.0, 1.0]
            return gl
        }
    }
}






extension UIColor {
    func mixin(infusion:UIColor, alpha:CGFloat) -> UIColor {
        let alpha2 = min(1.0, max(0, alpha))
        let beta = 1.0 - alpha2
        
        var r1:CGFloat = 0, r2:CGFloat = 0
        var g1:CGFloat = 0, g2:CGFloat = 0
        var b1:CGFloat = 0, b2:CGFloat = 0
        var a1:CGFloat = 0, a2:CGFloat = 0
        if getRed(&r1, green: &g1, blue: &b1, alpha: &a1) &&
            infusion.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)
        {
            let red     = r1 * beta + r2 * alpha2;
            let green   = g1 * beta + g2 * alpha2;
            let blue    = b1 * beta + b2 * alpha2;
            let alpha   = a1 * beta + a2 * alpha2;
            return UIColor(red: red, green: green, blue: blue, alpha: alpha)
        }
        // epique de las failuree
        return self
    }
}



















