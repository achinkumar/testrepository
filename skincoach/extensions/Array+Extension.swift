//
//  Array+Extension.swift
//  skincoach
//
//  Created by Achin Kumar on 06/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import Foundation

extension Array where Element == Float {
    var average: Double {
        return isEmpty ? 0.0 : Double(map { Double($0) }.reduce(0, +)) / Double(count)
    }
}

extension Array where Element == Int {
    var average: Double {
        return isEmpty ? 0.0 : Double(map { Double($0) }.reduce(0, +)) / Double(count)
    }
}

extension Array where Element == Double {
    var average: Double {
        return isEmpty ? 0.0 : Double(reduce(0, +)) / Double(count)
    }
}



extension Array where Element == Conditions {
    var average: Conditions {
        return Conditions(
            temperature: map { $0.temperature }.average,
            humidity: map { $0.humidity }.average,
            pressure: map { $0.pressure }.average,
            windSpeed: map { $0.windSpeed }.average,
            uvIndex: Int(map { $0.uvIndex }.average)
        )
    }
}


extension Array where Element == DayData {
    var positionOfToday: Int? {
        if (count > 0) {
            for i in 0..<count {
                if self[i].date.isToday { return i }
            }
        }
        return nil
    }
}


extension Array where Element : Hashable {
    var unique: [Element] {
        return Array(Set(self))
    }
}
