//
//  NSObject+Extension.swift
//  skincoach
//
//  Created by Achin Kumar on 20/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

extension UIView {
    static var identifier: String { return String(describing: self) }
    
    func addSubview(_ subview: UIView, withMargins margins: (leading: CGFloat, trailing: CGFloat, top: CGFloat, bottom: CGFloat)) -> (leading: NSLayoutConstraint, trailing: NSLayoutConstraint, top: NSLayoutConstraint, bottom: NSLayoutConstraint) {
        addSubview(subview)
        
        subview.translatesAutoresizingMaskIntoConstraints = false
        
        let leadingConstraint = NSLayoutConstraint(item: subview, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: margins.leading)
        let trailingConstraint = NSLayoutConstraint(item: subview, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: margins.trailing)
        let topConstraint = NSLayoutConstraint(item: subview, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: margins.top)
        let bottomConstraint = NSLayoutConstraint(item: subview, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: margins.bottom)
        self.addConstraints([leadingConstraint, trailingConstraint, topConstraint, bottomConstraint])
        return (leadingConstraint, trailingConstraint, topConstraint, bottomConstraint)
    }
}
