//
//  Int+Extension.swift
//  skincoach
//
//  Created by Achin Kumar on 08/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

extension Int {
    func times(_ f: () -> ()) {
        if self > 0 {
            for _ in 0..<self {
                f()
            }
        }
    }
    
    func times( f: @autoclosure () -> ()) {
        if self > 0 {
            for _ in 0..<self {
                f()
            }
        }
    }
    
    func shift(_ magnitude: Int = 2) -> Int {
        return self + Int.random(min: (0 - magnitude), max: magnitude)
    }
    
    
}


extension BinaryInteger {
    var degreesToRadians: CGFloat { return CGFloat(Int(self)) * .pi / 180 }
}

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}
