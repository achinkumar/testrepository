//
//  Activities.swift
//  skincoach
//
//  Created by Achin Kumar on 28/02/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit
import RealmSwift
import Charts

class Activity {
    let displayName: String
    let color: UIColor
    let info: String
    let id: String
    let labelName: String
    
    init(displayName: String, color: UIColor, info: String, id: String, labelName: String) {
        self.displayName = displayName
        self.color = color
        self.info = info
        self.id = id
        self.labelName = labelName
    }
    
    static let BocceBall = Activity(
        displayName: "Bocce Ball",
        color: UIColor.Activity.BocceBall,
        info: "BOCCE BALL IS YOUR HIGHEST UV ACTIVITY, BE SURE TO APPLY SUNSCREEN",
        id: "activity_bocce_ball",
        labelName: "Bocce Ball"
    )
    static let Basketball = Activity(
        displayName: "Basketball",
        color: UIColor.Activity.Basketball,
        info: "BASKETBALL IS YOUR HIGHEST UV ACTIVITY, BE SURE TO APPLY SUNSCREEN",
        id: "activity_basketball",
        labelName: "Basketball"
    )
    static let Gardening = Activity(
        displayName: "Gardening",
        color: UIColor.Activity.Gardening,
        info: "GARDENING IS YOUR HIGHEST UV ACTIVITY, BE SURE TO APPLY SUNSCREEN",
        id: "activity_gardening",
        labelName: "Gardening"
    )
    static let Kayaking = Activity(
        displayName: "Kayaking",
        color: UIColor.Activity.Kayaking,
        info: "KAYAKING IS YOUR HIGHEST UV ACTIVITY, BE SURE TO APPLY SUNSCREEN",
        id: "activity_kayaking",
        labelName: "Kayaking"
    )
    static let Running = Activity(
        displayName: "Running",
        color: UIColor.Activity.Running,
        info: "RUNNING IS YOUR HIGHEST UV ACTIVITY, BE SURE TO APPLY SUNSCREEN",
        id: "activity_running",
        labelName: "Running"
    )
    static let Swimming = Activity(
        displayName: "Swimming",
        color: UIColor.Activity.Swimming,
        info: "SWIMMING IS YOUR HIGHEST UV ACTIVITY, BE SURE TO APPLY SUNSCREEN",
        id: "activity_swimming",
        labelName: "Swimming"
    )
    static let Tennis = Activity(
        displayName: "Tennis",
        color: UIColor.Activity.Tennis,
        info: "TENNIS IS YOUR HIGHEST UV ACTIVITY, BE SURE TO APPLY SUNSCREEN",
        id: "activity_tennis",
        labelName: "Tennis"
    )
    static let Walking = Activity(
        displayName: "Walking",
        color: UIColor.Activity.Walking,
        info: "WALKING IS YOUR HIGHEST UV ACTIVITY, BE SURE TO APPLY SUNSCREEN",
        id: "activity_walking",
        labelName: "Walking"
    )
    
    static var list: [Activity] { return [BocceBall, Basketball, Gardening, Kayaking, Running, Swimming, Tennis, Walking] }
    
    static var random: Activity {
        switch Int.random(n: 7) {
        case 0: return BocceBall
        case 1: return Basketball
        case 2: return Gardening
        case 3: return Kayaking
        case 4: return Running
        case 5: return Swimming
        case 6: return Tennis
        default: return Walking
        }
    }
    
    static func forId(_ id: String) -> Activity {
        switch id {
        case BocceBall.id: return BocceBall
        case Basketball.id: return Basketball
        case Gardening.id: return Gardening
        case Kayaking.id: return Kayaking
        case Running.id: return Running
        case Swimming.id: return Swimming
        case Tennis.id: return Tennis
        default: return Walking
        }
    }
}






class PerformedActivity: Object {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var activityId: String = Activity.Running.id
    @objc dynamic var date: Date = Date()
    @objc dynamic var label: String = ""
    @objc dynamic var uvFrom: Double = 0.0
    @objc dynamic var uvTo: Double = 0.0
    @objc dynamic var isCurrent: Bool = false
    @objc dynamic var startConditions: Conditions? = nil
    @objc dynamic var endConditions: Conditions? = nil
    @objc dynamic var timeOfDay: String = "Morning"
    @objc dynamic var address: String = "Unknown"
    @objc dynamic var lat: Double = 0.0
    @objc dynamic var lng: Double = 0.0
    
    @objc dynamic var isPaused: Bool = false
    @objc dynamic var startedAtMS: Double = Double(Date().millisecondsSince1970)
    @objc dynamic var stoppedAtMS: Double = 0.0
    @objc dynamic var pausedAtMS: Double = 0.0
    @objc dynamic var totalPausedMS: Double = 0.0
    
    override static func primaryKey() -> String? { return "id" }
    
    convenience init(activityId: String, uvFrom: Double, uvTo: Double, date: Date, timeOfDay: String, address: String, startConditions: Conditions, endConditions: Conditions, lat: Double, lng: Double, startedAtMS: Double, stoppedAtMS: Double) {
        self.init()
        self.activityId = activityId
        self.uvFrom = uvFrom
        self.uvTo = uvTo
        self.date = date
        self.timeOfDay = timeOfDay
        self.address = address
        self.startConditions = startConditions
        self.endConditions = endConditions
        self.lat = lat
        self.lng = lng
        self.startedAtMS = startedAtMS
        self.stoppedAtMS = stoppedAtMS
    }
    
    static var all: [PerformedActivity] { return all().map { $0 as! PerformedActivity } }
    
    static var current: PerformedActivity? {
        get {
            let all = PerformedActivity.all.filter { $0.isCurrent == true }
            return all.count > 0 ? all[0] : nil
        }
        
        set {
            if let performedActivity = newValue {
                let previousPerformedActivity = current
                if let previousPerformedActivity = previousPerformedActivity {
                    Realm.write { previousPerformedActivity.isCurrent = false }
                }
                Realm.write { performedActivity.isCurrent = true }
            }
        }
    }
    
    var uvGainPercent: Float { return uvGain * 100.0 / User.def.skinTone.maxSunStock }
    var uvGain: Float {
        var gain = uvTo - uvFrom
        if gain < 0 { gain  = 0.0 }
        return Float(gain)
    }
    
    var runMS: Double {
        let lastMS = stoppedAtMS > 0.0 ? stoppedAtMS : Double(Date().millisecondsSince1970)
        return lastMS - startedAtMS - totalPausedMS
    }
    
    var timerStamp: String {
        let seconds = runMS / 1000.0
        let s = Int(seconds.truncatingRemainder(dividingBy: 60))
        let m = Int((seconds / 60).truncatingRemainder(dividingBy: 60))
        let h = Int((seconds / (60 * 60)).truncatingRemainder(dividingBy: 24))
        return "\(String(format: "%02d", h)):\(String(format: "%02d", m)):\(String(format: "%02d", s))"
    }
    
    var activity: Activity { return Activity.forId(activityId) }
    var stats: String { return "Started at \(Date(milliseconds: Int(startedAtMS)).HHMMA)" }
}



class CompositeActivityData {
    let activity: Activity
    var performedActivities = [PerformedActivity]()
    
    init(activity: Activity) { self.activity = activity }
    
    static var list: [CompositeActivityData] {
        return listFor(PerformedActivity.all)
    }
    
    static func listFor(_ performedActivities: [PerformedActivity]) -> [CompositeActivityData] {
        let performedActivities = performedActivities.reversed()
        var compositeActivities = [CompositeActivityData]()
        
        outer:for performedActivity in performedActivities {
            if performedActivity.isCurrent { continue }
            for compositeActivity in compositeActivities {
                if performedActivity.activityId == compositeActivity.activity.id {
                    compositeActivity.performedActivities.append(performedActivity)
                    continue outer
                }
            }
            let compositeActivity = CompositeActivityData(activity: performedActivity.activity)
            compositeActivity.performedActivities.append(performedActivity)
            compositeActivities.append(compositeActivity)
        }
        
        return compositeActivities
    }
    
    var statsText: String {
        var totalGainPercent: Float = 0
        for pa in performedActivities {
            totalGainPercent += pa.uvGainPercent
        }
        let averageGainPercent = Int(totalGainPercent / Float(performedActivities.count))
        return "\(performedActivities.count) \(performedActivities.count > 1 ? "Sessions" : "Session"), Avg \(averageGainPercent)% UV"
    }
}




class DayActivityData {
    var date: Date = Date()
    var averageConditions: Conditions? = nil
    var performedActivities = [PerformedActivity]()
    var events = [ChartDataEntry]()
    var address: String = ""

    var dateText: String { return date.same(date: Date()) ? "TODAY" : date.MMDDYY }
    var totalPercent: Float { return performedActivities.map { $0.uvGainPercent }.reduce(0, +) }
    var status: Status { return Status(uvPercent: totalPercent) }
    
    static var list: [DayActivityData] {
        let performedActivities = PerformedActivity.all().map { return $0 as! PerformedActivity }
        var dayActivities = [DayActivityData]()
        
        outer:for performedActivity in performedActivities {
            if !performedActivity.isCurrent {
                let performedActivityDate = Date(milliseconds: Int(performedActivity.startedAtMS))
                for dayActivity in dayActivities {
                    if performedActivityDate.same(date: dayActivity.date) {
                        dayActivity.performedActivities.append(performedActivity)
                        continue outer
                    }
                }
                let dayActivity = DayActivityData()
                dayActivity.date = performedActivityDate.startOfDay
                dayActivity.performedActivities.append(performedActivity)
                dayActivities.append(dayActivity)
            }
        }
        
        dayActivities.forEach {
            $0.averageConditions = $0.performedActivities.map { $0.endConditions! }.average
            var events = $0.events
            events.append(ChartDataEntry(x: 0.0, y: 0.0))
            var uvPercentSum = 0.0
            for i in 0...23 {
                $0.performedActivities.forEach { performedActivity in
                    if Date(milliseconds: Int(performedActivity.startedAtMS)).hour == i {
                        uvPercentSum += Double(performedActivity.uvGainPercent)
                        events.append(ChartDataEntry(x: Double(i), y: uvPercentSum))
                    }
                }
            }
            $0.address = $0.performedActivities[0].address
        }
        
        return dayActivities
    }
    
    
    
    class Status {
        let uvPercent: Float
        
        var iconName: String = ""
        var backgroundName: String = ""
        var message: String = ""
        var riskValue: String = ""
        var color = UIColor.black
        
        init(uvPercent: Float) {
            self.uvPercent = uvPercent
            
            if uvPercent >= 0 && uvPercent < 50 {
                iconName = ""
                backgroundName = ""
                message = Status.MESSAGE_LOW
                riskValue = "low"
                color = UIColor.Risk.Low
            } else if uvPercent >= 50 && uvPercent < 75 {
                iconName = ""
                backgroundName = ""
                message = Status.MESSAGE_MODERATE
                riskValue = "moderate"
                color = UIColor.Risk.Moderate
            } else if uvPercent >= 75 && uvPercent < 100 {
                iconName = ""
                backgroundName = ""
                message = Status.MESSAGE_HIGH
                riskValue = "high"
                color = UIColor.Risk.High
            } else {
                iconName = ""
                backgroundName = ""
                message = Status.MESSAGE_CRITICAL
                riskValue = "critical"
                color = UIColor.Risk.Critical
            }
        }
        
        static let MESSAGE_LOW = "WELL DONE! YOUR MORNING UV EXPOSURE IS THE LOWEST OF THE WEEK"
        static let MESSAGE_MODERATE = "WELL DONE! YOUR MORNING UV EXPOSURE IS THE LOWEST OF THE WEEK"
        static let MESSAGE_HIGH = "BE CAUTIOUS! YOUR UV EXPOSURE INCREASED 36% IN THE PAST HOUR"
        static let MESSAGE_CRITICAL = "BE CAUTIOUS! YOUR UV EXPOSURE INCREASED 36% IN THE PAST HOUR"
    }
}
























