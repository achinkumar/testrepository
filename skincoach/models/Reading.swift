//
//  Reading.swift
//  skincoach
//
//  Created by Achin Kumar on 26/02/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import Foundation
import RealmSwift
import Charts
import CoreLocation

class Conditions: Object {
    @objc dynamic var temperature: Double = 0
    @objc dynamic var humidity: Double = 0
    @objc dynamic var pressure: Double = 0
    @objc dynamic var windSpeed: Double = 0
    @objc dynamic var uvIndex: Int = -1
    
    convenience init(temperature: Double, humidity: Double, pressure: Double, windSpeed: Double, uvIndex: Int) {
        self.init()
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.windSpeed = windSpeed
        self.uvIndex = uvIndex
    }
}

enum Pass: Int {
    case BEFORE = 0, AFTER = 1
}


class Reading: Object {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var tagId: String = ""
    @objc dynamic var adc0: Int = 0
    @objc dynamic var adc1: Int = 0
    @objc dynamic var adc2: Int = 0
    @objc dynamic var temperature: Double = 0.0
    @objc dynamic var pass: Int = Pass.BEFORE.rawValue
    @objc dynamic var uva: Double = 0.0
    @objc dynamic var uvb: Double = 0.0
    @objc dynamic var isDummy: Bool = false
    
    convenience init(adc0: Int, pass: Pass) {
        self.init()
        self.adc0 = adc0
        self.pass = pass.rawValue
        addExposureData()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    private let UVA_TO_UVB_RATIO: Double = 25.0     // Hardcoded for Las Vegas
    private let UVA_A: Float = 0.000150             // MJ/m2 per mV (for Gain 2)
    
    func addExposureData() {
        uva = Double(UVA_A * Float(adc0))
        uvb = (uva / UVA_TO_UVB_RATIO)
    }
}

class ReadingPair: Object {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var initialReading: Reading? = nil
    @objc dynamic var finalReading: Reading? = nil
    @objc dynamic var uvGain: Double = 0.0
    @objc dynamic var isLast: Bool = false
    @objc dynamic var conditions: Conditions? = Conditions()
    @objc dynamic var address: String = ""
    @objc dynamic var latitude: Double = 0.0
    @objc dynamic var longitude: Double = 0.0
    @objc dynamic var date: Date = Date()
    
    var uvGainPercent: Float { return Float(uvGain * 100.0 / Double(User.def.skinTone.maxSunStock)) }
    func saveAsLast() { ReadingPair.last = self }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(initialReading: Reading?, finalReading: Reading?, conditions: Conditions? = nil, address: String? = nil, location: CLLocation? = nil, date: Date = Date()) {
        self.init()
        self.initialReading = initialReading
        self.finalReading = finalReading
        self.conditions = conditions ?? Cache.def.conditions ?? Conditions()
        self.address = address ?? Cache.def.address ?? "Unknown"
        if let location = location ?? Cache.def.location {
            self.latitude = location.coordinate.latitude
            self.longitude = location.coordinate.longitude
        }
        self.date = date
    }
    
    static var all: [ReadingPair] { return all().map { $0 as! ReadingPair } }
    
    static var last: ReadingPair? {
        get {
            if let readingPair = ReadingPair.one(where: "isLast == true") { return readingPair }
            return nil
        }
        
        set {
            if let readingPair = newValue {
                Realm.write {
                    if let lastReadingPair = last {
                        lastReadingPair.isLast = false
                        
                        if lastReadingPair.date.same(date: readingPair.date) {
                            readingPair.uvGain = readingPair.initialReading!.uva + readingPair.initialReading!.uvb - lastReadingPair.finalReading!.uva - lastReadingPair.finalReading!.uvb
                        }
                        if readingPair.uvGain < 1E-10 { readingPair.uvGain = 0.0 }
                    }
                    readingPair.isLast = true
                }
                
            }
        }
    }
}



enum RiskLevel {
    case low, moderate, high, critical
    
    static func forPercent(_ percent: Float) -> RiskLevel {
        if percent < 50.0 { return .low }
        if percent < 75.0 { return .moderate }
        if percent < 100.0 { return .high }
        else { return .critical }
    }
    
    var text: String {
        switch self {
        case .low:      return "LOW"
        case .moderate: return "MODERATE"
        case .high:     return "HIGH"
        case .critical: return "CRITICAL"
        }
    }
    
    var color: UIColor {
        switch self {
        case .low:      return UIColor.Risk.Low
        case .moderate: return UIColor.Risk.Moderate
        case .high:     return UIColor.Risk.High
        case .critical: return UIColor.Risk.Critical
        }
    }
}



class DaySectionDistribution {
    var morningPercent: Int
    var afternoonPercent: Int
    var eveningPercent: Int
    var nightPercent: Int
    
    init(readingPairs: [ReadingPair]) {
        var morningGain = 0.0
        var afternoonGain = 0.0
        var eveningGain = 0.0
        var nightGain = 0.0
        var totalGain = 0.0
        
        readingPairs.forEach { readingPair in
            switch readingPair.date.timeOfDay {
            case "Morning": morningGain += readingPair.uvGain; break
            case "Afternoon": afternoonGain += readingPair.uvGain; break
            case "Evening": eveningGain += readingPair.uvGain; break
            default: nightGain += readingPair.uvGain
            }
            totalGain += readingPair.uvGain
        }
        
        morningPercent = morningGain == 0.0 ? 0 : Int(ceil(morningGain * 100.0 / totalGain))
        afternoonPercent = afternoonGain == 0.0 ? 0 : Int(ceil(afternoonGain * 100.0 / totalGain))
        eveningPercent = eveningGain == 0.0 ? 0 : Int(ceil(eveningGain * 100.0 / totalGain))
        nightPercent = nightGain == 0.0 ? 0 : Int(ceil(nightGain * 100.0 / totalGain))
        
        if morningPercent + afternoonPercent + eveningPercent + nightPercent > 100 {
            let excess = morningPercent + afternoonPercent + eveningPercent + nightPercent - 100
            if morningPercent > afternoonPercent && morningPercent > eveningPercent && morningPercent > nightPercent {
                morningPercent -= excess
            } else if afternoonPercent > eveningPercent && afternoonPercent > nightPercent {
                afternoonPercent -= excess
            } else if eveningPercent > nightPercent {
                eveningPercent -= excess
            } else {
                nightPercent -= excess
            }
        }
    }
}







protocol PerspectiveData {
    var dateText: String { get }
    var events: [ChartDataEntry] { get }
    var compositeActivities: [CompositeActivityData] { get }
    var daySectionDistribution: DaySectionDistribution { get }
}



class DayData: PerspectiveData {
    var date: Date = Date()
    var readingPairs = [ReadingPair]()
    var averageConditions: Conditions { return createAverageConditions() }
    var address: String { return readingPairs.count > 0 ? readingPairs[0].address : "" }
    var totalPercent: Float { return readingPairs.map { $0.uvGainPercent }.reduce(0, +) }
    var status: Status { return Status(uvPercent: totalPercent) }
    
    var highestUV: Int { return Int(readingPairs.map { $0.conditions != nil ? $0.conditions!.uvIndex : -1 }.max()!) }
    var highestTemp: Int {
        let ht = readingPairs.map { $0.conditions != nil ? $0.conditions!.temperature : Double.leastNormalMagnitude }.max()!
        return ht == Double.leastNormalMagnitude ? Int.min : Int(ht)
    }
    var totalUVGain: Int { return Int(readingPairs.map { $0.uvGain }.reduce(0, +) * 1000) }
    var performedActivities: [PerformedActivity] { return PerformedActivity.all.filter { return Date(milliseconds: Int($0.stoppedAtMS)).same(date: date) } }
    
    var riskLevel: RiskLevel { return RiskLevel.forPercent(totalPercent) }
    
    // Perspective Data ----------------------------------------------------------------------
    var dateText: String { return date.same(date: Date()) ? "TODAY" : date.MMDDYY }
    
    var events: [ChartDataEntry] {
        var events = [ChartDataEntry]()
        events.append(ChartDataEntry(x: 0.0, y: 0.0))
        var uvPercentSum = 0.0
        
        for hour in 0...23 {
            for readingPair in readingPairs {
                if(readingPair.date.hour == hour) {
                    uvPercentSum += Double(readingPair.uvGainPercent)
                }
            }
            events.append(ChartDataEntry(x: Double(hour), y: uvPercentSum))
        }
        return events
    }
    
    private var daySectionDistributionInstance: DaySectionDistribution? = nil
    var daySectionDistribution: DaySectionDistribution {
        daySectionDistributionInstance = daySectionDistributionInstance ?? DaySectionDistribution(readingPairs: readingPairs)
        return daySectionDistributionInstance!
    }
    
    var compositeActivities: [CompositeActivityData] { return CompositeActivityData.listFor(performedActivities) }
    
    // -------------------------------------------------------------------------------------
    
    
    
    private func createAverageConditions() -> Conditions {
        var temperatureTotal = 0.0
        var humidityTotal = 0.0
        var pressureTotal = 0.0
        var uvIndexTotal = 0.0
        var windSpeedTotal = 0.0
        
        var count = 0.0
        
        for readingPair in readingPairs {
            if(readingPair.finalReading!.isDummy || readingPair.conditions == nil) { continue }
            
            count += 1
            
            temperatureTotal += readingPair.conditions!.temperature
            humidityTotal += readingPair.conditions!.humidity
            pressureTotal += readingPair.conditions!.pressure
            uvIndexTotal += Double(readingPair.conditions!.uvIndex)
            windSpeedTotal += readingPair.conditions!.windSpeed
        }
        
        if(count > 0) {
            return Conditions(
                temperature : temperatureTotal / count,
                humidity    : (humidityTotal / count) * 100,
                pressure    : pressureTotal / count,
                windSpeed   : windSpeedTotal / count,
                uvIndex     : Int(uvIndexTotal / count)
            )
        } else {
            return Conditions(
                temperature : Double.leastNormalMagnitude,
                humidity    : 0.0,
                pressure    : 0.0,
                windSpeed   : 0.0,
                uvIndex     : -1
            )
        }
    }
    
    static var list: [DayData] {
        var readingPairs = ReadingPair.all
        var dayReadings = [DayData]()
        
        if(readingPairs.count == 0) {
            let firstDummyReading = Reading()
            let secondDummyReading = Reading()
            let dummyReadingPair = ReadingPair(initialReading: firstDummyReading, finalReading: secondDummyReading)
            readingPairs.append(dummyReadingPair)
        }
        
        outer:for readingPair in readingPairs {
            let readingDate = readingPair.date
            for dayReading in dayReadings {
                if readingDate.same(date: dayReading.date) {
                    dayReading.readingPairs.append(readingPair)
                    continue outer
                }
            }
            let dayReading = DayData()
            dayReading.date = readingDate.startOfDay
            dayReading.readingPairs.append(readingPair)
            dayReadings.append(dayReading)
        }
        
        return dayReadings
    }
    
    
    class Status {
        let uvPercent: Float
        var iconImageName: String = ""
        var backgroundImageName: String = ""
        var message: String = ""
        var riskValue: String = ""
        var color = UIColor.blue
        
        init(uvPercent: Float) {
            self.uvPercent = uvPercent
            if uvPercent >= 0 && uvPercent < 50 {
                iconImageName = ""
                backgroundImageName = ""
                message = MESSAGE_LOW
                riskValue = "low"
                color = UIColor.Risk.Low
            } else if uvPercent >= 50 && uvPercent < 75 {
                iconImageName = ""
                backgroundImageName = ""
                message = MESSAGE_MODERATE
                riskValue = "moderate"
                color = UIColor.Risk.Moderate
            } else if uvPercent >= 75 && uvPercent < 100 {
                iconImageName = ""
                backgroundImageName = ""
                message = MESSAGE_HIGH
                riskValue = "high"
                color = UIColor.Risk.High
            } else {
                iconImageName = ""
                backgroundImageName = ""
                message = MESSAGE_CRITICAL
                riskValue = "critical"
                color = UIColor.Risk.Critical
            }
        }
        
        let MESSAGE_LOW = "You have been minimally exposed to UV, you are at a low risk for damage".uppercased()
        let MESSAGE_MODERATE = "You have received some exposure, so remain cautious in the sun".uppercased()
        let MESSAGE_HIGH = "You have received a lot of exposure, remain cautious to prevent damage".uppercased()
        let MESSAGE_CRITICAL = "You have exceeded your maximum UV dose, remain cautious in the sun!".uppercased()
    }
}







class WeekData: PerspectiveData {
    var startDate: Date = Date()
    var readingPairs = [ReadingPair]()
    
    var avgGainPercent: Int { return Int(daywiseData.map { $0.totalPercent }.average) }
    var highestGainPercent: Int { return Int(daywiseData.map { $0.totalPercent }.max()!) }
    var lowestGainPercent: Int { return Int(daywiseData.map { $0.totalPercent }.min()!) }
    
    var performedActivities: [PerformedActivity] { return PerformedActivity.all.filter { Date(milliseconds: Int($0.stoppedAtMS)).sameWeek(date: startDate) } }
    
    
    // Perspective Data ----------------------------------------------------------------------
    var dateText: String {
        if startDate.same(date: Date().startOfWeek) { return "THIS WEEK" }
        else { return "\(startDate.MMDDYY) - \(startDate.endOfWeek.minus(days: 1).MMDDYY)" }
    }
    
    var events: [ChartDataEntry] { return createEvents() }
    
    private var daySectionDistributionInstance: DaySectionDistribution? = nil
    var daySectionDistribution: DaySectionDistribution {
        daySectionDistributionInstance = daySectionDistributionInstance ?? DaySectionDistribution(readingPairs: readingPairs)
        return daySectionDistributionInstance!
    }
    
    var compositeActivities: [CompositeActivityData] { return CompositeActivityData.listFor(performedActivities) }
    // ---------------------------------------------------------------------------------------
    
    
    private var cachedEvents: [ChartDataEntry]? = nil
    private func createEvents() -> [ChartDataEntry] {
        if cachedEvents != nil { return cachedEvents! }
        var events = [ChartDataEntry]()
        for dayOfWeek in 1...7 {
            var uvPercentSum:Float = 0
            for readingpair in readingPairs {
                if readingpair.date.dayOfWeek == dayOfWeek {
                    uvPercentSum += readingpair.uvGainPercent
                }
            }
            events.append(ChartDataEntry(x: Double(dayOfWeek), y: Double(uvPercentSum)))
        }
        cachedEvents = events
        return events
    }
    
    
    private var cachedDaywiseData: [DayData]? = nil
    private var daywiseData: [DayData] {
        if cachedDaywiseData != nil { return cachedDaywiseData! }
        var dayReadings = [DayData]()
        outer:for readingPair in readingPairs {
            let readingDate = readingPair.date
            for dayReading in dayReadings {
                if readingDate.same(date: dayReading.date) {
                    dayReading.readingPairs.append(readingPair)
                    continue outer
                }
            }
            let dayReading = DayData()
            dayReading.date = readingDate.startOfDay
            dayReading.readingPairs.append(readingPair)
            dayReadings.append(dayReading)
        }
        cachedDaywiseData = dayReadings
        return dayReadings
    }
    
    
    static var list: [WeekData] {
        var weekReadings = [WeekData]()
        var readingPairs = [ReadingPair]()
        readingPairs.append(contentsOf: ReadingPair.all)
        
        if readingPairs.count == 0 {
            let firstDummyReading = Reading()
            let secondDummyReading = Reading()
            let dummyReadingPair = ReadingPair(initialReading: firstDummyReading, finalReading: secondDummyReading)
            readingPairs.append(dummyReadingPair)
        }
        
        outer:for readingPair in readingPairs {
            let readingDate = readingPair.date
            for weekReading in weekReadings {
                if readingDate.sameWeek(date: weekReading.startDate) {
                    weekReading.readingPairs.append(readingPair)
                    continue outer
                }
            }
            
            let weekReading = WeekData()
            weekReading.startDate = readingDate.startOfWeek
            weekReading.readingPairs.append(readingPair)
            weekReadings.append(weekReading)
        }
        
        return weekReadings
    }
}







class MonthData: PerspectiveData {
    var startDate: Date = Date()
    var readingPairs = [ReadingPair]()
    
    var avgGainPercent: Int { return Int(daywiseData.map { $0.totalPercent }.average) }
    var highestGainpercent: Int { return Int(daywiseData.map { $0.totalPercent }.max()!) }
    var lowestGainpercent: Int { return Int(daywiseData.map { $0.totalPercent }.min()!) }
    
    var performedActivities: [PerformedActivity] { return PerformedActivity.all.filter { Date(milliseconds: Int($0.stoppedAtMS)).sameMonth(date: startDate) } }
    
    
    // Perspective Data ----------------------------------------------------------------------
    var dateText: String {
        if startDate.same(date: Date().startOfMonth) { return "THIS MONTH" }
        else if startDate.sameYear(date: Date()) {
            return "\(startDate.MMMM)".uppercased()
        } else {
            return "\(startDate.MMMMYYYY)".uppercased()
        }
    }
    
    var events: [ChartDataEntry] { return createEvents() }
    
    private var daySectionDistributionInstance: DaySectionDistribution? = nil
    var daySectionDistribution: DaySectionDistribution {
        daySectionDistributionInstance = daySectionDistributionInstance ?? DaySectionDistribution(readingPairs: readingPairs)
        return daySectionDistributionInstance!
    }
    
    var compositeActivities: [CompositeActivityData] { return CompositeActivityData.listFor(performedActivities) }
    // ---------------------------------------------------------------------------------------
    
    
    private var cachedEvents: [ChartDataEntry]? = nil
    private func createEvents() -> [ChartDataEntry] {
        if cachedEvents != nil { return cachedEvents! }
        var events = [ChartDataEntry]()
        for dayOfMonth in 1...startDate.daysInMonth {
            var uvPercentSum = 0.0
            for readingPair in readingPairs {
                if readingPair.date.day == dayOfMonth {
                    uvPercentSum += Double(readingPair.uvGainPercent)
                }
            }
            events.append(ChartDataEntry(x: Double(dayOfMonth), y: uvPercentSum))
        }
        cachedEvents = events
        return events
    }
    
    private var cachedDaywiseData: [DayData]? = nil
    private var daywiseData: [DayData] {
        if cachedDaywiseData != nil { return cachedDaywiseData! }
        var dayReadings = [DayData]()
        outer:for readingPair in readingPairs {
            let readingDate = readingPair.date
            for dayReading in dayReadings {
                if readingDate.same(date: dayReading.date) {
                    dayReading.readingPairs.append(readingPair)
                    continue outer
                }
            }
            let dayReading = DayData()
            dayReading.date = readingDate.startOfDay
            dayReading.readingPairs.append(readingPair)
            dayReadings.append(dayReading)
        }
        cachedDaywiseData = dayReadings
        return dayReadings
    }
    
    
    static var list: [MonthData] {
        var monthReadings = [MonthData]()
        var readingPairs = [ReadingPair]()
        readingPairs.append(contentsOf: ReadingPair.all)
        
        if readingPairs.count == 0 {
            let firstDummyReading = Reading()
            let secondDummyReading = Reading()
            let dummyReadingPair = ReadingPair(initialReading: firstDummyReading, finalReading: secondDummyReading)
            readingPairs.append(dummyReadingPair)
        }
        
        outer:for readingPair in readingPairs {
            let readingDate = readingPair.date
            for monthReading in monthReadings {
                if readingDate.sameMonth(date: monthReading.startDate) {
                    monthReading.readingPairs.append(readingPair)
                    continue outer
                }
            }
            let monthReading = MonthData()
            monthReading.startDate = readingDate.startOfMonth
            monthReading.readingPairs.append(readingPair)
            monthReadings.append(monthReading)
        }
        return monthReadings
    }
}























































