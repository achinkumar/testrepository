//
//  User.swift
//  skincoach
//
//  Created by Achin Kumar on 28/02/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit
import RealmSwift

enum Gender: String {
    case male = "Male"
    case female = "Female"
}

enum SkinType: String {
    case normal = "Normal"
    case combination = "Combination"
    case oily = "Oily"
    case dry = "Dry"
    case sensitive = "Sensitive"
    
    static var options: [String] { return [SkinType.normal, SkinType.combination, SkinType.oily, SkinType.dry, SkinType.sensitive].map {return $0.rawValue} }
}

class SkinTone {
    let value: String
    let phototype: Int
    let color: UIColor
    let maxSunStock: Float
    
    init(value: String, phototype: Int, color: UIColor, maxSunStock: Float) {
        self.value = value
        self.phototype = phototype
        self.color = color
        self.maxSunStock = maxSunStock
    }
    
    static let Ivory = SkinTone(
        value: "Ivory",
        phototype: 1,
        color: UIColor.Skintone.Ivory,
        maxSunStock: 0.03
    )
    static let Fair = SkinTone(
        value: "Fair",
        phototype: 2,
        color: UIColor.Skintone.Fair,
        maxSunStock: 0.044
    )
    static let Beige = SkinTone(
        value: "Beige",
        phototype: 3,
        color: UIColor.Skintone.Beige,
        maxSunStock: 0.058
    )
    static let Olive = SkinTone(
        value: "Olive",
        phototype: 4,
        color: UIColor.Skintone.Olive,
        maxSunStock: 0.074
    )
    static let Dark = SkinTone(
        value: "Dark",
        phototype: 5,
        color: UIColor.Skintone.Dark,
        maxSunStock: 0.088
    )
    static let VeryDark = SkinTone(
        value: "Very Dark",
        phototype: 6,
        color: UIColor.Skintone.VeryDark,
        maxSunStock: 0.088
    )
    
    static func forPhototype(_ phototype: Int) -> SkinTone {
        switch phototype {
        case Ivory.phototype: return Ivory
        case Fair.phototype: return Fair
        case Beige.phototype: return Beige
        case Olive.phototype: return Olive
        case Dark.phototype: return Dark
        default: return VeryDark
        }
    }
    
    static func forValue(_ value: String) -> SkinTone {
        switch value {
        case Ivory.value: return Ivory
        case Fair.value: return Fair
        case Beige.value: return Beige
        case Olive.value: return Olive
        case Dark.value: return Dark
        default: return VeryDark
        }
    }
}




class User: Object {
    private static let DEFAULT_USER_ID = "__default__user__"
    
    @objc dynamic var id: String = DEFAULT_USER_ID
    @objc dynamic var name: String = "Taylor Smith"
    @objc dynamic var birthdate: Date = Date().minus(years: 20)
    @objc dynamic var rawGender: String = Gender.male.rawValue
    @objc dynamic var rawSkinTone: String = SkinTone.Ivory.value
    @objc dynamic var rawSkinType: String = SkinType.normal.rawValue
    @objc dynamic var sunscreen: String = "Lotion"
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    var gender: Gender {
        get { return Gender.init(rawValue: rawGender)! }
        set { rawGender = newValue.rawValue }
    }
    
    var skinTone: SkinTone {
        get { return SkinTone.forValue(rawSkinTone) }
        set { rawSkinTone = newValue.value }
    }
    
    var skinType: SkinType {
        get { return SkinType.init(rawValue: rawSkinType)! }
        set { rawSkinType = newValue.rawValue }
    }
    
    static var def: User {
        if let defaultUser = User.one() { return defaultUser }
        let user = User()
        user.save()
        return user
    }
}


























