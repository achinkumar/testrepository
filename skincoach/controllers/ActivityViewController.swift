//
//  ActivityViewController.swift
//  skincoach
//
//  Created by Achin Kumar on 20/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit
import RealmSwift

class ActivityViewController: BaseViewController {
    
    static var runningInstance: ActivityViewController? {
        if let performedActivity = PerformedActivity.current {
            let vc = UIStoryboard.vc(self)
            vc.performedActivity = performedActivity
            vc.activity = performedActivity.activity
            return vc
        }
        return nil
    }
    
    static func newInstance(withActivity activity: Activity) -> ActivityViewController {
        let vc = UIStoryboard.vc(self)
        vc.activity = activity
        return vc
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var riskLevelLabel: UILabel!
    
    @IBOutlet weak var playStopButtonsView: UIView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    
    @IBOutlet weak var tipBackground: UIImageView!
    @IBOutlet weak var tipIcon: UIImageView!
    @IBOutlet weak var tipText: UILabel!
    
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var uvIndexLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var pollenLevelLabel: UILabel!
    @IBOutlet weak var airQualityLabel: UILabel!
    
    private var activity: Activity!
    private var performedActivity: PerformedActivity?
    private var reader: NDEFReader!
    
    override var preferredLeftMode: LeftButtonMode { return performedActivity == nil ? .back : .none }
    override var preferredRightMode: RightButtonMode { return .none }
    override var preferTabBarHidden: Bool { return true }
    override var preferredBottomStripColor: UIColor { return UIColor.Base.LightBlue }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tick()
        
        setConditionsData()
        Cache.refresh { (hasUpdated) in if hasUpdated { self.setConditionsData() } }
    }
    
    @IBAction func start(_ sender: UIButton) {
        reader = NDEFReader.instance
        reader.read(onResult: { (readingPair) in
            self.initialize(withReadingPair: readingPair)
        }) { (error) in
            self.showAlert(withTitle: nil, andMessage: error)
        }
    }
    
    @IBAction func stop(_ sender: UIButton) {
        reader = NDEFReader.instance
        reader.read(onResult: { (readingPair) in
            self.stop(withReadingPair: readingPair)
        }) { (error) in
            self.showAlert(withTitle: nil, andMessage: error)
        }
    }
    
    @IBAction func play(_ sender: UIButton) {
        Realm.write {
            performedActivity!.isPaused = false
            let pausedFor = Double(Date().millisecondsSince1970) - performedActivity!.pausedAtMS
            performedActivity!.totalPausedMS += pausedFor
            performedActivity!.pausedAtMS = 0.0
            performedActivity!.isPaused = false
        }
        tick()
    }
    
    @IBAction func pause(_ sender: UIButton) {
        Realm.write {
            performedActivity!.pausedAtMS = Double(Date().millisecondsSince1970)
            performedActivity!.isPaused = true
        }
        refreshUI()
    }
    
    private func refreshUI() {
        nameLabel.text = activity.displayName.uppercased()
        tipText.text = activity.info
        timerLabel.text = performedActivity?.timerStamp ?? "00:00:00"
        timerLabel.alpha = performedActivity?.isPaused ?? true ? 0.4 : 1.0
        
        startButton.isHidden = performedActivity != nil
        playStopButtonsView.isHidden = performedActivity == nil || !performedActivity!.isPaused
        pauseButton.isHidden = performedActivity == nil || performedActivity!.isPaused
    }
    
    private func setConditionsData() {
        if let conditions = Cache.def.conditions {
            uvIndexLabel.text = conditions.uvIndex >= 0 ? "\(conditions.uvIndex)" : "n/a"
            humidityLabel.text = conditions.uvIndex >= 0 ? "\(Int(conditions.humidity * 100))" : "n/a"
            temperatureLabel.text = conditions.uvIndex >= 0 ? "\(Int(conditions.temperature))" : "n/a"
            pollenLevelLabel.text = conditions.uvIndex >= 0 ? "9.4" : "n/a"
            airQualityLabel.text = conditions.uvIndex >= 0 ? "59" : "n/a"
        }
        
        locationLabel.text = Cache.def.address ?? "Unknown"
    }
    
    private func tick() {
        UI { self.refreshUI() }
        if performedActivity != nil && !performedActivity!.isPaused { Dispatch.after(seconds: 0.1) { self.tick() } }
    }
    
    private func initialize(withReadingPair readingPair: ReadingPair) {
        let defCache = Cache.def
        performedActivity = PerformedActivity()
        performedActivity!.activityId = activity.id
        performedActivity!.uvFrom = readingPair.finalReading!.uva + readingPair.finalReading!.uvb
        performedActivity!.timeOfDay = Date().timeOfDay
        performedActivity!.address = defCache.address ?? "Unknown"
        performedActivity!.lat = defCache.location?.coordinate.latitude ?? 0.0
        performedActivity!.lng = defCache.location?.coordinate.longitude ?? 0.0
        performedActivity!.startConditions = defCache.conditions ?? Conditions()
        performedActivity!.save()
        PerformedActivity.current = performedActivity
        tick()
        
        containerVC?.setup(leftButtonMode: LeftButtonMode.none)
    }
    
    private func stop(withReadingPair readingPair: ReadingPair) {
        Realm.write {
            performedActivity!.stoppedAtMS = Double(Date().millisecondsSince1970)
            performedActivity!.isCurrent = false
            performedActivity!.uvTo = readingPair.initialReading!.uva + readingPair.initialReading!.uvb
            performedActivity!.endConditions = Cache.def.conditions ?? Conditions()
        }
        Dispatch.after(seconds: 0.1) {
            if let nvc = self.navigationController {
                nvc.setViewControllers([nvc.viewControllers[0]], animated: true)
            }
        }
    }
    
}





