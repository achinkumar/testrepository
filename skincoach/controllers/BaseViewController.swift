//
//  BaseViewController.swift
//  skincoach
//
//  Created by Achin Kumar on 13/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit
import KeepBackgroundCell

class BaseTableViewCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        keepSubviewBackground = true
        
        let selectedBackgroundView = UIView()
        selectedBackgroundView.backgroundColor = UIColor.Common.OffWhite
        self.selectedBackgroundView = selectedBackgroundView
    }
}

class BaseViewController: UIViewController, UINavigationControllerDelegate {
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    enum AnimationStyle {
        case none
        case dissolve
        case slideOverBottom
    }
    
    var preferredLeftMode: LeftButtonMode { return .profile }
    var preferredRightMode: RightButtonMode { return .coach }
    var screenTitle: String? { return nil }
    var preferredTabIconTintColor: UIColor { return UIColor.Base.PrimaryDark }
    var preferTabBarHidden: Bool { return false }
    var preferredBottomStripColor: UIColor { return UIColor.white }
    
    var containerVC: ContainerViewController? {
        var vc: UIViewController = self
        repeat {
            if vc.parent is ContainerViewController { return vc.parent as? ContainerViewController }
            vc = vc.parent!
        } while(vc.parent != nil)
        return nil
    }
    
    var tbc: TabBarController? {
        var vc: UIViewController = self
        repeat {
            if vc.parent is TabBarController { return vc.parent as? TabBarController }
            vc = vc.parent!
        } while(vc.parent != nil)
        return nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let cvc = containerVC {
            cvc.setup(withTitle: screenTitle, leftButtonMode: preferredLeftMode, rightButtonMode: preferredRightMode, bottomStripColor: preferredBottomStripColor)
        }
        if let tbc = tabBarController {
            tbc.tabBar.tintColor = preferredTabIconTintColor
            
            if preferTabBarHidden {
                hideTabBar()
            } else {
                showTabBar()
            }
        }
    }
    
    var animationStyle : AnimationStyle = .none
    
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return animationController(type: operation)
    }
    
    func animationController(type : UINavigationControllerOperation) -> BaseAnimator? {
        switch animationStyle {
        case .none:
            return nil
        case .dissolve:
            return DissolveAnimator(operation: type)
        case .slideOverBottom:
            return SlideOverBottomAnimator(operation: type)
        }
    }
    
    func hideTabBar() {
        hideTabBar(withDuration: 0.3)
//        setTabBarVisible(visible: false, animated: true)
    }
    
    func hideTabBarInstantly() {
        hideTabBar(withDuration: 0.0)
//        setTabBarVisible(visible: false, animated: false)
    }
    
    private func hideTabBar(withDuration duration: CGFloat) {
        if (self.tabBarController?.tabBar.isHidden)! {
            return
        }
        
        self.tabBarController?.tabBar.isHidden = true
        
        let offset: CGFloat = 50.0
        UIView.animate(withDuration: TimeInterval(duration), animations: {
            self.tabBarController?.tabBar.center = CGPoint(x: (self.tabBarController?.tabBar.center.x)!, y: (self.tabBarController?.tabBar.center.y)! + offset)
            self.tabBarController?.tabBar.alpha = 0
        }, completion:{(_) in
            self.tabBarController?.tabBar.isHidden = true
        })
    }
    
    func showTabBar() {
        if (self.tabBarController?.tabBar.isHidden)! == false {
            return
        }
        
        self.tabBarController?.tabBar.isHidden = false
        
        let offset: CGFloat = -50.0
        UIView.animate(withDuration: 0.3, animations: {
            self.tabBarController?.tabBar.center = CGPoint(x: (self.tabBarController?.tabBar.center.x)!, y: (self.tabBarController?.tabBar.center.y)! + offset)
            self.tabBarController?.tabBar.isHidden = false
            self.tabBarController?.tabBar.alpha = 1
        })
    }
    
    
    
    
    func showAlert(withTitle title: String?, andMessage message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func push(_ vc: UIViewController) { push(vc: vc, animated: true) }
    func pushInstantly(_ vc: UIViewController) { push(vc: vc, animated: false) }
    private func push(vc: UIViewController, animated: Bool) { navigationController?.pushViewController(vc, animated: animated) }
    
    
    
    
    
    
    
    
}
