//
//  TrendsViewController.swift
//  skincoach
//
//  Created by Achin Kumar on 13/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit
import Charts

class TrendsHeaderCell: BaseTableViewCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dayIndicator: UIView!
    @IBOutlet weak var weekLabel: UILabel!
    @IBOutlet weak var weekIndicator: UIView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var monthIndicator: UIView!
    
    @IBOutlet weak var navigationLabel: UILabel!
    @IBOutlet weak var goPreviousButton: UIButton!
    @IBOutlet weak var goNextButton: UIButton!
    
    @IBOutlet weak var parameter1ValueLabel: UILabel!
    @IBOutlet weak var parameter1KeyLabel: UILabel!
    @IBOutlet weak var parameter2ValueLabel: UILabel!
    @IBOutlet weak var parameter2KeyLabel: UILabel!
    @IBOutlet weak var parameter3ValueLabel: UILabel!
    @IBOutlet weak var parameter3KeyLabel: UILabel!
    
    @IBOutlet weak var dayChart: LineChartView!
    @IBOutlet weak var weekChart: BarChartView!
    @IBOutlet weak var monthChart: BarChartView!
    
    @IBOutlet weak var dayChartFooter: UIView!
    @IBOutlet weak var weekChartFooter: UIStackView!
    @IBOutlet weak var monthChartFooter: UIStackView!
    
    @IBOutlet weak var morningLabel: UILabel!
    @IBOutlet weak var afternoonLabel: UILabel!
    @IBOutlet weak var eveningLabel: UILabel!
    @IBOutlet weak var nightLabel: UILabel!
    
    @IBOutlet weak var morningProgressView: ColorProgressView!
    @IBOutlet weak var afternoonProgressView: ColorProgressView!
    @IBOutlet weak var eveningProgressView: ColorProgressView!
    @IBOutlet weak var nightProgressView: ColorProgressView!
    
    @IBOutlet weak var tipLabel: UILabel!
    
    var perspectiveSwitchDelegate: PerspectiveSwitchDelegate?
    var perspectiveDataList: [PerspectiveData]!
    var selectedPerspective: Perspective!
    var currentPage = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup(forPerspective: Perspective.Day)
    }
    
    @IBAction func selectDayPerspective(_ sender: UIButton) {
        if selectedPerspective != Perspective.Day {
            setup(forPerspective: Perspective.Day)
        }
    }
    
    @IBAction func selectWeekPerspective(_ sender: UIButton) {
        if selectedPerspective != Perspective.Week {
            setup(forPerspective: Perspective.Week)
        }
    }
    
    @IBAction func selectMonthPerspective(_ sender: UIButton) {
        if selectedPerspective != Perspective.Month {
            setup(forPerspective: Perspective.Month)
        }
    }
    
    @IBAction func goPrevious(_ sender: UIButton) {
        set(page: currentPage - 1)
    }
    
    @IBAction func goNext(_ sender: UIButton) {
        set(page: currentPage + 1)
    }
    
    func setup(forPerspective perspective: Perspective, onPage page: Int? = nil) {
        selectedPerspective = perspective
        switch perspective {
        case .Day   : perspectiveDataList = DayData.list; break
        case .Week  : perspectiveDataList = WeekData.list; break
        case .Month : perspectiveDataList = MonthData.list; break
        }
        goNextButton.isEnabled = false
        goPreviousButton.isEnabled = perspectiveDataList.count > 1
        set(page: page ?? perspectiveDataList.count - 1)
    }
    
    private func set(page: Int) {
        currentPage = page
        goPreviousButton.isEnabled = currentPage != 0
        goNextButton.isEnabled = currentPage < perspectiveDataList.count - 1
        goPreviousButton.alpha = currentPage == 0 ? 0.5 : 1.0
        goNextButton.alpha = currentPage == perspectiveDataList.count - 1 ? 0.5 : 1.0
        
        let perspectiveData = perspectiveDataList[page]
        
        navigationLabel.text = perspectiveData.dateText
        
        if perspectiveData is DayData {
            parameter1KeyLabel.text = "HIGHEST\nUV INDEX"
            parameter2KeyLabel.text = "HIGHEST\nTEMP"
            parameter3KeyLabel.text = "TOTAL UV\nEXPOSURE"
        } else {
            parameter1KeyLabel.text = "AVG %"
            parameter2KeyLabel.text = "HIGHEST %"
            parameter3KeyLabel.text = "LOWEST %"
        }
        
        let daySectionDistribution = perspectiveData.daySectionDistribution
        morningLabel.text = "\(daySectionDistribution.morningPercent)"
        afternoonLabel.text = "\(daySectionDistribution.afternoonPercent)"
        eveningLabel.text = "\(daySectionDistribution.eveningPercent)"
        nightLabel.text = "\(daySectionDistribution.nightPercent)"
        
        morningProgressView.set(progress: Float(daySectionDistribution.morningPercent))
        afternoonProgressView.set(progress: Float(daySectionDistribution.afternoonPercent))
        eveningProgressView.set(progress: Float(daySectionDistribution.eveningPercent))
        nightProgressView.set(progress: Float(daySectionDistribution.nightPercent))
        
        if let dayData = perspectiveData as? DayData {
            setupForDayPerspective()
            
            let highestUV = dayData.highestUV
            let highestTemp = dayData.highestTemp
            parameter1ValueLabel.text = highestUV >= 0 ? "\(highestUV)" : "0"
            parameter2ValueLabel.text = highestTemp != Int.min ? "\(highestTemp)" : "0"
            parameter3ValueLabel.text = "\(dayData.totalUVGain)"
            
            let events = dayData.events
            let yMax = max(100.0, events.map { $0.y }.max()!)
            dayChart.setup(entries: events, gridColor: UIColor.white, lineColors: [UIColor.Chart.LightYellow, UIColor.Chart.DarkOrange], yMax: Float(yMax))
            
        } else if let weekData = perspectiveData as? WeekData {
            setupForWeekPerspective()
            
            parameter1ValueLabel.text = "\(weekData.avgGainPercent)"
            parameter2ValueLabel.text = "\(weekData.highestGainPercent)"
            parameter3ValueLabel.text = "\(weekData.lowestGainPercent)"
            
            let events = weekData.events
            let yMax = max(100.0, events.map { $0.y }.max()!)
            weekChart.setup(entries: events, gridColor: UIColor.clear, yMax: Float(yMax))
            
        } else if let monthData = perspectiveData as? MonthData {
            setupForMonthPerspective()
            
            parameter1ValueLabel.text = "\(monthData.avgGainPercent)"
            parameter2ValueLabel.text = "\(monthData.highestGainpercent)"
            parameter3ValueLabel.text = "\(monthData.lowestGainpercent)"
            
            let events = monthData.events
            let yMax = max(100.0, events.map { $0.y }.max()!)
            monthChart.setup(entries: events, gridColor: UIColor.clear, yMax: Float(yMax))
        }
        
        if let perspectiveSwitchDelegate = perspectiveSwitchDelegate {
            perspectiveSwitchDelegate.perspectiveDataChangedTo(perspectiveData, perspective: selectedPerspective, onPage: currentPage)
        }
    }
    
    private func setupForDayPerspective() {
        dayChart.isHidden = false
        weekChart.isHidden = true
        monthChart.isHidden = true
        
        dayChartFooter.isHidden = false
        weekChartFooter.isHidden = true
        monthChartFooter.isHidden = true
    
        dayIndicator.isHidden = false
        weekIndicator.isHidden = true
        monthIndicator.isHidden = true
    
        dayLabel.alpha = 1.0
        weekLabel.alpha = 0.3
        monthLabel.alpha = 0.3
    }
    
    private func setupForWeekPerspective() {
        dayChart.isHidden = true
        weekChart.isHidden = false
        monthChart.isHidden = true
        
        dayChartFooter.isHidden = true
        weekChartFooter.isHidden = false
        monthChartFooter.isHidden = true
        
        dayIndicator.isHidden = true
        weekIndicator.isHidden = false
        monthIndicator.isHidden = true
        
        dayLabel.alpha = 0.3
        weekLabel.alpha = 1.0
        monthLabel.alpha = 0.3
    }
    
    private func setupForMonthPerspective() {
        dayChart.isHidden = true
        weekChart.isHidden = true
        monthChart.isHidden = false
        
        dayChartFooter.isHidden = true
        weekChartFooter.isHidden = true
        monthChartFooter.isHidden = false
        
        dayIndicator.isHidden = true
        weekIndicator.isHidden = true
        monthIndicator.isHidden = false
        
        dayLabel.alpha = 0.3
        weekLabel.alpha = 0.3
        monthLabel.alpha = 1.0
    }
}

protocol PerspectiveSwitchDelegate {
    func perspectiveDataChangedTo(_ perspectiveData: PerspectiveData, perspective: Perspective, onPage page: Int)
}


enum Perspective {
    case Day, Week, Month
    
    var dataList: [PerspectiveData] {
        switch self {
        case .Day: return DayData.list
        case .Week: return WeekData.list
        case .Month: return MonthData.list
        }
    }
}

class TrendsPerformedActivitiesCell: BaseTableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var indicator: UIView!
}

class TrendsViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var compositeActivities = [CompositeActivityData]()
    var selectedPerspective: Perspective = Perspective.Day
    
    private var currentPage = -1
    private var shouldRefreshHeader = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let perspectiveData = selectedPerspective.dataList
        if currentPage == -1 { currentPage = perspectiveData.count - 1 }
        compositeActivities = perspectiveData[currentPage].compositeActivities
        shouldRefreshHeader = true
        
        // Refresh tableview without animation or scroll
        UIView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        tableView.reloadData()
        tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
        
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    }
}

extension TrendsViewController: PerspectiveSwitchDelegate {
    func perspectiveDataChangedTo(_ perspectiveData: PerspectiveData, perspective: Perspective, onPage page: Int) {
        compositeActivities = perspectiveData.compositeActivities
        currentPage = page
        selectedPerspective = perspective
        tableView.reloadData()
    }
}

extension TrendsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return compositeActivities.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: TrendsHeaderCell.identifier, for: indexPath) as! TrendsHeaderCell
            cell.perspectiveSwitchDelegate = self
            if shouldRefreshHeader {
                shouldRefreshHeader = false
                cell.setup(forPerspective: selectedPerspective, onPage: currentPage)
            }
            return cell
        } else {
            let compositeActivity = compositeActivities[indexPath.row - 1]
            let cell = tableView.dequeueReusableCell(withIdentifier: TrendsPerformedActivitiesCell.identifier, for: indexPath) as! TrendsPerformedActivitiesCell
            cell.nameLabel.text = compositeActivity.activity.labelName.capitalized
            cell.indicator.backgroundColor = compositeActivity.activity.color
            cell.summaryLabel.text = compositeActivity.statsText
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 { return 538 }
        else { return 70 }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row > 0 {
            push(PerformedActivitiesViewController.instance(forPerformedActivities: compositeActivities[indexPath.row - 1].performedActivities))
        }
    }
}



































