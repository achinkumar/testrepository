//
//  PerformedActivitiesViewController.swift
//  skincoach
//
//  Created by Achin Kumar on 22/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

class PerformedActivitiesCell: BaseTableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var colorIndicator: UIView!
    @IBOutlet weak var uvPercentLabel: UILabel!
    
}

class PerformedActivitiesViewController: BaseViewController {
    @IBOutlet weak var performedActivitiesList: UITableView!
    
    private var performedActivities: [PerformedActivity]!
    private var header: String!
    
    static func instance(forPerformedActivities performedActivities: [PerformedActivity]) -> PerformedActivitiesViewController {
        let vc = UIStoryboard.vc(self)
        vc.performedActivities = performedActivities
        vc.header = performedActivities.first!.activity.displayName.uppercased()
        return vc
    }
    
    override var preferredLeftMode: LeftButtonMode { return .back }
    override var preferredRightMode: RightButtonMode { return .none }
    override var screenTitle: String? { return header }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        performedActivitiesList.delegate = self
        performedActivitiesList.dataSource = self
    }
}

extension PerformedActivitiesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return performedActivities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let performedActivity = performedActivities[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: PerformedActivitiesCell.identifier, for: indexPath) as! PerformedActivitiesCell
        cell.nameLabel.text = "\(performedActivity.timeOfDay) \(performedActivity.activity.labelName)"
        cell.dateLabel.text = performedActivity.date.MMDDYY
        cell.timeLabel.text = performedActivity.date.HHMMA
        cell.uvPercentLabel.text = "\(Int(performedActivity.uvGainPercent))% UV"
        cell.colorIndicator.backgroundColor = performedActivity.activity.color
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        push(PerformedActivityDetailViewController.instance(withPerformedActivity: performedActivities[indexPath.row]))
    }
}
