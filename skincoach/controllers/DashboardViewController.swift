//
//  DashboardViewController.swift
//  skincoach
//
//  Created by Achin Kumar on 13/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit
import Charts

class DashboardDetailCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var arcProgressView: ArcProgressView!
    @IBOutlet weak var uvPercentLabel: UILabel!
    @IBOutlet weak var uvBreakdownLabel: UILabel!
    @IBOutlet weak var riskLevelLabel: UILabel!
    
    @IBOutlet weak var uvIndexLabel: UILabel!
    @IBOutlet weak var currentHumidityLabel: UILabel!
    @IBOutlet weak var currentTempLabel: UILabel!
    @IBOutlet weak var pollenLevelLabel: UILabel!
    @IBOutlet weak var airQualityLabel: UILabel!
    
    func set(forDayData dayData: DayData) {
        arcProgressView.set(progress: dayData.totalPercent / 100.0)
        uvPercentLabel.text = "\(Int(dayData.totalPercent))"
        
        let unitBreakdown = dayData.totalPercent / 26.0
        let uvaPercentage = Int(round(25.0 * unitBreakdown))
        let uvbPercentage = Int(round(1.0 * unitBreakdown))
        
        uvBreakdownLabel.text = "\(uvaPercentage)% UVA / \(uvbPercentage)% UVB"
        
        riskLevelLabel.text = dayData.riskLevel.text
        riskLevelLabel.textColor = dayData.riskLevel.color
        
        let averageConditions = dayData.averageConditions
        
        uvIndexLabel.text = averageConditions.uvIndex >= 0 ? "\(averageConditions.uvIndex)" : "0"
        currentHumidityLabel.text = averageConditions.uvIndex >= 0 ? "\(Int(averageConditions.humidity))" : "0"
        currentTempLabel.text = averageConditions.uvIndex >= 0 ? "\(Int(averageConditions.temperature))" : "0"
        pollenLevelLabel.text = averageConditions.uvIndex >= 0 ? "9.4" : "0"
        airQualityLabel.text = averageConditions.uvIndex >= 0 ? "59" : "0"
    }
}

class DashboardViewController: BaseViewController {

    @IBOutlet weak var warningView: UIView!
    @IBOutlet weak var warningIconImage: UIImageView!
    @IBOutlet weak var warningText: UILabel!
    @IBOutlet weak var warningBorderView: NSLayoutConstraint!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var previousDateButton: UIButton!
    @IBOutlet weak var nextDateButton: UIButton!
    
    @IBOutlet weak var detailCollectionView: UICollectionView!
    
    @IBOutlet weak var lineChartView: LineChartView!
    
    var currentPage = 0
    var daysData: [DayData]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        daysData = DayData.list.reversed()
        setupForCurrentPage()
    }

    @IBAction func showPreviousDate(_ sender: UIButton) {
        currentPage += 1
        setupForCurrentPage()
    }
    
    @IBAction func showNextDate(_ sender: UIButton) {
        currentPage -= 1
        setupForCurrentPage()
    }
    
    private func setupCollectionView() {
        detailCollectionView.backgroundColor = UIColor.clear
        detailCollectionView.backgroundView = UIView(frame: CGRect.zero)
        detailCollectionView.delegate = self
        detailCollectionView.dataSource = self
    }
    
    private func setupForCurrentPage() {
        previousDateButton.isEnabled = currentPage < daysData.count - 1
        nextDateButton.isEnabled = currentPage > 0
        detailCollectionView.reloadData()
        dateLabel.text = daysData[currentPage].dateText
        
        var yMax = 0.0
        for event in daysData[currentPage].events {
            yMax = max(yMax, event.y)
        }
        if yMax == 0.0 { yMax = 10.0 }
        
        lineChartView.setup(entries: daysData[currentPage].events, gridColor: UIColor.white, yMax: Float(yMax))
    }
}

extension DashboardViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dayData = daysData[currentPage]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DashboardDetailCollectionViewCell.self), for: indexPath) as! DashboardDetailCollectionViewCell
        cell.set(forDayData: dayData)
        return cell
    }
}

































