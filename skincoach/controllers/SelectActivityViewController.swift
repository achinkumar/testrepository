//
//  SelectActivityViewController.swift
//  skincoach
//
//  Created by Achin Kumar on 19/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

class ActivityTypeListCell: BaseTableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var colorIndicator: UIView!
}

class SelectActivityViewController: BaseViewController {
    @IBOutlet weak var activityTypeList: UITableView!
    var activities: [Activity]!
    
    static var instance: SelectActivityViewController { return UIStoryboard.vc(self) }
    
    override var preferredLeftMode: LeftButtonMode { return .back }
    override var preferredRightMode: RightButtonMode { return .none }
    override var screenTitle: String? { return "Select Activity" }
    override var preferTabBarHidden: Bool { return true }
    override var preferredBottomStripColor: UIColor { return UIColor.Base.LightBlue }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activities = Activity.list
        activityTypeList.delegate = self
        activityTypeList.dataSource = self
    }
}

extension SelectActivityViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let activity = activities[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: ActivityTypeListCell.identifier, for: indexPath) as! ActivityTypeListCell
        cell.nameLabel.text = activity.labelName
        cell.colorIndicator.backgroundColor = activity.color
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let activity = activities[indexPath.row]
        push(ActivityViewController.newInstance(withActivity: activity))
    }
}
