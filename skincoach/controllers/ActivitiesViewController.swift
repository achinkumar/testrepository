//
//  ActivitiesViewController.swift
//  skincoach
//
//  Created by Achin Kumar on 13/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

class PerformedActivityListCell: BaseTableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var colorIndicator: UIView!
}

class ActivitiesViewController: BaseViewController {

    @IBOutlet weak var performedActivityList: UITableView!
    private var compositeActivities: [CompositeActivityData]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        compositeActivities = CompositeActivityData.list
        performedActivityList.delegate = self
        performedActivityList.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        compositeActivities = CompositeActivityData.list
        performedActivityList.reloadData()
    }
    
    @IBAction func startSession(_ sender: UIButton) {
        push(SelectActivityViewController.instance)
    }
}

extension ActivitiesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return compositeActivities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let compositeActivity = compositeActivities[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PerformedActivityListCell.self), for: indexPath) as! PerformedActivityListCell
        cell.nameLabel.text = compositeActivity.activity.labelName
        cell.colorIndicator.backgroundColor = compositeActivity.activity.color
        cell.summaryLabel.text = compositeActivity.statsText
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        push(PerformedActivitiesViewController.instance(forPerformedActivities: compositeActivities[indexPath.row].performedActivities))
    }
}
