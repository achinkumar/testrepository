//
//  SkinToneSelectorViewController.swift
//  skincoach
//
//  Created by Achin Kumar on 14/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

class SkinToneSelectorViewController: BaseViewController {
    
    var onSelection: ((SkinTone) -> Void)?
    
    static func show(in vc: BaseViewController, _ onSelection: ((SkinTone) -> Void)? = nil) {
        let ins = instance
        ins.onSelection = onSelection
        vc.push(ins)
    }
    
    private static var instance: SkinToneSelectorViewController { return UIStoryboard.vc(self) }
    
    override var screenTitle: String? { return "Select Skin Tone" }
    override var preferredLeftMode: LeftButtonMode { return LeftButtonMode.back }
    override var preferredRightMode: RightButtonMode{ return RightButtonMode.none }
    override var preferredTabIconTintColor: UIColor { return UIColor.Base.Accent }
    override var preferTabBarHidden: Bool { return true }
    override var preferredBottomStripColor: UIColor { return UIColor.Base.LightBlue }
    
    @IBAction func selectIvory(_ sender: UIButton) { select(.Ivory) }
    @IBAction func selectFair(_ sender: UIButton) { select(.Fair) }
    @IBAction func selectBeige(_ sender: UIButton) { select(.Beige) }
    @IBAction func selectOlive(_ sender: UIButton) { select(.Olive) }
    @IBAction func selectDark(_ sender: UIButton) { select(.Dark) }
    @IBAction func selectVeryDark(_ sender: UIButton) { select(.VeryDark) }
    
    private func select(_ skinTone: SkinTone) {
        if let onSelection = onSelection {
            onSelection(skinTone)
        }
        navigationController?.popViewController(animated: true)
    }
}
