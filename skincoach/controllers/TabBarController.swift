//
//  TabBarController.swift
//  skincoach
//
//  Created by Achin Kumar on 13/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Dispatch.eoq {
            if let currentlyPerformedActivityVC = ActivityViewController.runningInstance {
                if let nvc = self.selectedViewController as? UINavigationController {
                    nvc.pushViewController(currentlyPerformedActivityVC, animated: false)
                }
            }
        }
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name:"AvenirNext-Medium", size:9.5)!, NSAttributedStringKey.foregroundColor: UIColor.black], for:.normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name:"AvenirNext-Medium", size:9.5)!, NSAttributedStringKey.foregroundColor: UIColor.black], for:.selected)
        
        self.tabBar.tintColor = UIColor.Base.PrimaryDark
        self.tabBar.barTintColor = UIColor.white
        
        self.tabBar.isTranslucent = true
        
        delegate = self
        
        if let items = self.tabBar.items {
            for item in items {
                if let image = item.image {
                    item.image = image.withRenderingMode( .alwaysOriginal )
                    item.titlePositionAdjustment.vertical = -1
                }
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let newTabBarHeight = defaultTabBarHeight + 4.0
        
        var newFrame = tabBar.frame
        newFrame.size.height = newTabBarHeight
        newFrame.origin.y = view.frame.size.height - newTabBarHeight
        
        tabBar.frame = newFrame
    }
    
    private lazy var defaultTabBarHeight = { self.tabBar.frame.size.height }()
    
    func showProfileScreen() {
        if let nvc = selectedViewController as? UINavigationController {
            let pvc = ProfileViewController.instance
            pvc.animationStyle = .slideOverBottom
            nvc.pushViewController(pvc, animated: true)
        }
    }
    
    func showSkintoneSelector() {
        
    }
    
    func showActivitySelector() {
        if let nvc = selectedViewController as? UINavigationController {
            nvc.pushViewController(SelectActivityViewController.instance, animated: true)
        }
    }
    
    func goBack() {
        if let nvc = selectedViewController as? UINavigationController {
            if nvc.viewControllers.count > 1 {
                nvc.popViewController(animated: true)
            }
        }
    }
}


extension TabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let nvc = viewController as? UINavigationController {
            if nvc.viewControllers.count > 0 {
                let rootVC = nvc.viewControllers[0]
                switch tabBarController.selectedIndex {
                case 0:
//                    if let featuredVC = rootVC as? FeaturedViewController {
//                        featuredVC.tableView.setContentOffset(CGPoint.zero, animated: true)
//                    }
                    break
                case 1:
                    
                    break
                case 2:
                    
                    break
                case 3:
                    
                    break
                case 4:
                    
                    break
                default: break
                }
            }
            
            nvc.setViewControllers([nvc.viewControllers.first!], animated: false)
        }
    }
}
