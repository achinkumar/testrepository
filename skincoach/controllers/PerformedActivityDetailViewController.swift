//
//  PerformedActivityDetailViewController.swift
//  skincoach
//
//  Created by Achin Kumar on 23/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit
import Charts
import CoreLocation
import MapKit

class PerformedActivityDetailViewController: BaseViewController {
    
    static func instance(withPerformedActivity performedActivity: PerformedActivity) -> PerformedActivityDetailViewController {
        let vc = UIStoryboard.vc(self)
        vc.performedActivity = performedActivity
        vc.header = "\(performedActivity.timeOfDay) \(performedActivity.activity.labelName)".uppercased()
        return vc
    }
    
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var mapView: MapViewWithZoom!
    
    @IBOutlet weak var uvIndexLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var pollenLevelLabel: UILabel!
    @IBOutlet weak var airQualityLabel: UILabel!
    
    @IBOutlet weak var exposureChart: LineChartView!
    @IBOutlet weak var exposureStartTimeLabel: UILabel!
    @IBOutlet weak var exposureEndTimeLabel: UILabel!
    
    @IBOutlet weak var indexChart: LineChartView!
    @IBOutlet weak var indexStartTimeLabel: UILabel!
    @IBOutlet weak var indexEndTimeLabel: UILabel!
    
    private var performedActivity: PerformedActivity!
    private var header: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateTimeLabel.text = "\(performedActivity.date.DDMMYY) at \(performedActivity.date.HHMMA)"
        locationLabel.text = performedActivity.address
        
        let conditions = performedActivity.startConditions ?? Conditions()
        uvIndexLabel.text = conditions.uvIndex >= 0 ? "\(conditions.uvIndex)" : "0"
        humidityLabel.text = conditions.uvIndex >= 0 ? "\(Int(conditions.humidity * 100))" : "0"
        temperatureLabel.text = conditions.uvIndex >= 0 ? "\(Int(conditions.temperature))" : "0"
        pollenLevelLabel.text = conditions.uvIndex >= 0 ? "9.4" : "0"
        airQualityLabel.text = conditions.uvIndex >= 0 ? "59" : "0"
        
        var uveEntries = [ChartDataEntry]()
        uveEntries.append(ChartDataEntry(x: 0.0, y: 3.0))
        uveEntries.append(ChartDataEntry(x: 100.0, y: Double(performedActivity.uvGainPercent) + 3.0))
        
        var uviEntries = [ChartDataEntry]()
        uviEntries.append(ChartDataEntry(x: 0.0, y: Double(performedActivity.startConditions?.uvIndex ?? 0)))
        uviEntries.append(ChartDataEntry(x: 100.0, y: Double(performedActivity.startConditions?.uvIndex ?? 0)))
        
        var yMax = uveEntries.map {$0.y}.max() ?? 100.0
        let yMin = uveEntries.map {$0.y}.min() ?? 0.0
        
        if yMax < 100.0 { yMax = 100.0 }
        
        exposureChart.setup(entries: uveEntries, gridColor: UIColor.Base.LightBlueAlt, lineColors: [UIColor.Chart.LightYellow, UIColor.Chart.DarkOrange], yMin: Float(yMin), yMax: Float(yMax))
        indexChart.setup(entries: uviEntries, gridColor: UIColor.Base.LightBlueAlt, yMax: 12)
        
        exposureStartTimeLabel.text = Date(milliseconds: Int(performedActivity.startedAtMS)).HHMMA
        exposureEndTimeLabel.text = Date(milliseconds: Int(performedActivity.stoppedAtMS)).HHMMA
        
        indexStartTimeLabel.text = Date(milliseconds: Int(performedActivity.startedAtMS)).HHMMA
        indexEndTimeLabel.text = Date(milliseconds: Int(performedActivity.stoppedAtMS)).HHMMA
        
        let location = CLLocation(latitude: performedActivity.lat, longitude: performedActivity.lng)
        mapView.setCenterCoordinate(coordinate: location.coordinate, zoomLevel: 12, animated: false)
        
        let annotation = MKPointAnnotation()
        let centerCoordinate = location.coordinate
        annotation.coordinate = centerCoordinate
        mapView.addAnnotation(annotation)
    }
    
    override var preferredLeftMode: LeftButtonMode { return .back }
    override var preferredRightMode: RightButtonMode { return .none }
    override var screenTitle: String? { return header }
}















