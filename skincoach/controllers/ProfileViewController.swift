//
//  ProfileViewController.swift
//  skincoach
//
//  Created by Achin Kumar on 13/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit
import RealmSwift

class ProfileViewController: BaseViewController, UITextFieldDelegate {

    static var instance: ProfileViewController { return UIStoryboard.vc(self) }
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabelTop: UILabel!
    @IBOutlet weak var prototypeValueLabel: UILabel!
    @IBOutlet weak var nameEditor: UITextField!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var skintoneLabel: UILabel!
    @IBOutlet weak var skintoneIndicator: CircleView!
    @IBOutlet weak var skinTypeLabel: UILabel!
    @IBOutlet weak var sunscreenLabel: UILabel!
    
    @IBOutlet weak var nameButton: UIButton!
    
    private var user: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        user = User.def
        
        nameLabelTop.text = user.name
        nameEditor.text = user.name
        prototypeValueLabel.text = "\(user.skinTone.phototype)"
        birthdayLabel.text = user.birthdate.DDMMYY
        genderLabel.text = user.gender.rawValue.capitalized
        skintoneLabel.text = user.skinTone.value.capitalized
        skintoneIndicator.set(color: user.skinTone.color)
        skinTypeLabel.text = user.skinType.rawValue.capitalized
        
        nameEditor.delegate = self
        
        hideKeyboardWhenTappedAround()
    }
    
    override var preferredLeftMode: LeftButtonMode { return LeftButtonMode.back }
    override var preferredRightMode: RightButtonMode{ return RightButtonMode.none }
    override var preferredTabIconTintColor: UIColor { return UIColor.Base.Accent }

    @IBAction func onNameChanged(_ sender: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        saveNameFromEditor()
        nameEditor.resignFirstResponder()
        return true
    }
    
    private func saveNameFromEditor() {
        if let text = nameEditor.text, text.count > 0 {
            Realm.write { user.name = text }
            nameLabelTop.text = text
        } else {
            nameEditor.text = user.name
        }
    }
    
    @IBAction func selectBirthday(_ sender: UIButton) {
        stopEditingNameIfIsEditing()
        if let cvc = containerVC {
            let _ = DateSelectorView.show(inViewController: cvc, withTitle: "Select Birthday", withSelectedDate: user.birthdate, andMaxDate: Date(), andMinDate: Date().minus(years: 100), withOnDone: { (selectedDate) in
                Realm.write { self.user.birthdate = selectedDate }
                self.birthdayLabel.text = selectedDate.DDMMYY
            })
        }
    }
    
    @IBAction func selectGender(_ sender: UIButton) {
        stopEditingNameIfIsEditing()
        if let cvc = containerVC {
            let _ = PickerContainerView.show(inViewController: cvc, withTitle: "Select Gender", withDisplayData: ["Male", "Female"], andSelectedValue: user.gender.rawValue, withOnDone: { (value, pos) in
                Realm.write { self.user.gender = value == "Male" ? .male : .female }
                self.genderLabel.text = value
            })
        }
    }
    
    @IBAction func selectSkintone(_ sender: UIButton) {
        stopEditingNameIfIsEditing()
        hideTabBar()
        Dispatch.after(seconds: 0) {
            SkinToneSelectorViewController.show(in: self) { (skinTone) in
                Realm.write { self.user.skinTone = skinTone }
                self.skintoneLabel.text = skinTone.value.capitalized
                self.skintoneIndicator.set(color: skinTone.color)
                self.prototypeValueLabel.text = "\(skinTone.phototype)"
            }
        }
        
    }
    
    @IBAction func selectSkinType(_ sender: UIButton) {
        stopEditingNameIfIsEditing()
        if let cvc = containerVC {
            let _ = PickerContainerView.show(inViewController: cvc, withTitle: "Select Skin Type", withDisplayData: SkinType.options, andSelectedValue: user.skinType.rawValue, withOnDone: { (value, pos) in
                Realm.write { self.user.skinType = SkinType(rawValue: value)! }
                self.skinTypeLabel.text = value
            })
        }
    }
    
    @IBAction func selectSunscreen(_ sender: UIButton) {
    }
    
    private func stopEditingNameIfIsEditing() {
        if nameEditor.isEditing {
            saveNameFromEditor()
            nameEditor.resignFirstResponder()
        }
    }
    
    private func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private  func dismissKeyboard() {
        view.endEditing(true)
        saveNameFromEditor()
    }
}
