//
//  ContainerViewController.swift
//  skincoach
//
//  Created by Achin Kumar on 13/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit
import RealmSwift

enum LeftButtonMode {
    case back, profile, none
    var imageName: String { return self == .none ? "" : self == .back ? "BackIcon" : "ProfileIcon" }
}

enum RightButtonMode {
    case coach, none
}

class ContainerViewController: UIViewController {
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var coachButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var appTitleImageView: UIImageView!
    @IBOutlet weak var appTitleLabel: UILabel!
    @IBOutlet weak var bottomStrip: UIView!
    var tabBarVC: TabBarController!
    var canGoBack = false    
    
    @IBAction func showProfile(_ sender: UIButton) {
        if canGoBack {
            tabBarVC.goBack()
        } else {
            tabBarVC.showProfileScreen()
        }
    }
    
    @IBAction func showCoach(_ sender: UIButton) {
        tabBarVC.showActivitySelector()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for childViewController in childViewControllers {
            if childViewController is TabBarController {
                tabBarVC = childViewController as! TabBarController
                break
            }
        }
    }
    
    func setup(withTitle title: String? = nil, leftButtonMode leftMode: LeftButtonMode? = nil, rightButtonMode rightMode: RightButtonMode? = nil, bottomStripColor bottomColor: UIColor? = nil) {
        if let title = title {
            appTitleImageView.isHidden = true
            appTitleLabel.text = title
        } else {
            appTitleImageView.isHidden = false
            appTitleLabel.text = ""
        }
        
        if let leftMode = leftMode {
            canGoBack = leftMode == .back
            if leftMode == .none {
                profileButton.isHidden = true
            } else {
                profileButton.isHidden = false
                profileButton.setImage(UIImage(named: leftMode.imageName), for: .normal)
            }
        }
        
        if let rightMode = rightMode {
            coachButton.isHidden = rightMode == .none
        }
        
        if let bottomColor = bottomColor {
            bottomStrip.backgroundColor = bottomColor
        }
    }
}
























