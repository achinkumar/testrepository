//
//  SplashViewController.swift
//  skincoach
//
//  Created by Achin Kumar on 15/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Dispatch.after(seconds: 1) {
            self.performSegue(withIdentifier: "ShowTabBarVC", sender: self)
        }
    }
}
