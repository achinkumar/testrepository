//
//  ScanViewController.swift
//  skincoach
//
//  Created by Achin Kumar on 13/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import UIKit
import CoreNFC
import Toast

class ScanViewController: BaseViewController, NFCNDEFReaderSessionDelegate {

    var reader: NDEFReader!
    
    static func show(inVc vc: BaseViewController, withOnResult onResult: @escaping (_ initialReading: Reading, _ finalReading: Reading) -> Void, onError: @escaping (_ message: String) -> Void) -> ScanViewController {
        let vc = UIStoryboard.vc(self)
        vc.onResult = onResult
        vc.onError = onError
        return vc
    }
    
    var nfcSession: NFCNDEFReaderSession?
    var shouldShowTabBar = true
    
    var onResult: ((_ initialReading: Reading, _ finalReading: Reading) -> Void)?
    var onError: ((_ message: String) -> Void)?
    
    override var preferTabBarHidden: Bool { return !shouldShowTabBar }
    
    @IBAction func sync(_ sender: UIButton) {
        if Config.manualInterventionSupported {
            var tf: UITextField!
            let alert = UIAlertController(title: "Input Delay", message: "This is for how long the NFC system pop-up should stay active.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addTextField(configurationHandler: {(textField: UITextField!) in
                tf = textField
                textField.placeholder = "Enter seconds (eg: '4.5')"
                textField.keyboardType = .decimalPad
            })
            alert.addAction(UIAlertAction(title: "START SCAN", style: .default, handler: { (action) in
                let delay: Float? = tf.text == nil ? nil : Float(tf.text!)
                
                self.reader = NDEFReader()
                self.reader.read(forSeconds: delay, onResult: { (readingPair) in
                    self.showAlert(withTitle: nil, andMessage: "Initial: \(readingPair.initialReading!.adc0) mV \nFinal : \(readingPair.finalReading!.adc0) mV")
                }) { (errorMessage) in
                    self.showAlert(withTitle: "Failure!", andMessage: errorMessage)
                }
                
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            reader = NDEFReader.instance
            reader.read(onResult: { (readingPair) in
                self.tbc?.selectedIndex = 0
                self.containerVC?.view.makeToast("Reading saved successfully!")
            }) { (errorMessage) in
                self.showAlert(withTitle: "Failure!", andMessage: errorMessage)
            }
        }
    }
    
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        print("The session was invalidated: \(error.localizedDescription)")
    }

    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        print("message received")
    }
}
