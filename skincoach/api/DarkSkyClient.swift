//
//  DarkSkyClient.swift
//  skincoach
//
//  Created by Achin Kumar on 22/03/18.
//  Copyright © 2018 vinsol. All rights reserved.
//

import Alamofire
import CoreLocation
import EVReflection

class DarkSkyData: EVObject {
    var currently = Current()
}

class Current: EVObject {
    var time: Int = 0
    var summary: String = ""
    var icon: String = ""
    var temperature: Double = 0.0
    var humidity: Double = 0.0
    var pressure: Double = 0.0
    var windSpeed: Double = 0.0
    var uvIndex: Int = 0
}


class DarkSkyClient {
    typealias URLRequestParameters = [String: Any]
    static var shared = DarkSkyClient()
    
    func conditions(forLocation location: CLLocation, _ onResult: @escaping (Conditions?) -> Void) {
        Alamofire.request(Router.Conditions(location))
            .validate()
            .responseJSON { (response) in
                if response.result.isSuccess, let dict = response.result.value as? NSDictionary {
                    let data = DarkSkyData(dictionary: dict)
                    onResult(Conditions(temperature: data.currently.temperature,
                                        humidity: data.currently.humidity,
                                        pressure: data.currently.pressure,
                                        windSpeed: data.currently.windSpeed,
                                        uvIndex: data.currently.uvIndex))
                }
                onResult(nil)
        }
    }
}

extension DarkSkyClient {
    enum Router: URLRequestConvertible {
        case Conditions(CLLocation)
        
        var method: Alamofire.HTTPMethod { return .get }
        var path: String { switch self { case .Conditions(let location): return "forecast/915ea8fec56d741770c1f7df9835d387/\(location.coordinate.latitude),\(location.coordinate.longitude)" }}
        
        func asURLRequest() throws -> URLRequest {
            var urlRequest = URLRequest(url: URL(string: "https://api.darksky.net/\(path)")!)
            urlRequest.httpMethod = method.rawValue
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            return urlRequest
        }
    }
}

